/*
 * Licensed to Marvelution under one or more contributor license 
 * agreements.  See the NOTICE file distributed with this work 
 * for additional information regarding copyright ownership.
 * Marvelution licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package com.marvelution.bamboo.plugins.sonar.upgrade;

import static com.marvelution.bamboo.plugins.sonar.utils.SonarConfigurationHelper.*;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.internal.verification.VerificationModeFactory;

import com.atlassian.bamboo.bandana.PlanAwareBandanaContext;
import com.atlassian.bamboo.build.Build;
import com.atlassian.bamboo.build.BuildDefinition;
import com.atlassian.bamboo.build.BuildManager;
import com.atlassian.bandana.BandanaManager;
import com.marvelution.bamboo.plugins.sonar.BambooSonarServer;
import com.marvelution.bamboo.plugins.sonar.BandanaUtils;

/**
 * Testcase for {@link SonarCleanupUpgradeTask}
 * 
 * @author <a href="mailto:markrekveld@marvelution.com">Mark Rekveld</a>
 */
public class SonarCleanupUpgradeTaskTest {

	private SonarCleanupUpgradeTask task;

	@Mock
	private BandanaManager bandanaManager;

	@Mock
	private BuildManager buildManager;

	@Mock
	private BuildDefinition buildDefinition;

	@Mock
	private Build build;

	/**
	 * Setup the task variables
	 * 
	 * @throws Exception in case of errors
	 */
	@Before
	public void before() throws Exception {
		MockitoAnnotations.initMocks(this);
		task = new SonarCleanupUpgradeTask(buildManager, bandanaManager);
	}

	/**
	 * Test {@link SonarFixSonarServersUpgradeTask#doUpgrade()}
	 * 
	 * @throws Exception in case of errors
	 */
	@Test
	public void testDoUpgrade() throws Exception {
		final Map<String, String> config = new HashMap<String, String>();
		config.put(SONAR_RUN, "true");
		config.put(SONAR_SERVER, BUILD_PLAN_SPECIFIC);
		when(buildManager.getAllBuildsForEdit()).thenReturn(Collections.singletonList(build));
		when(build.getBuildDefinition()).thenReturn(buildDefinition);
		when(build.getName()).thenReturn("Test Build");
		when(buildDefinition.getCustomConfiguration()).thenReturn(config);
		task.doUpgrade();
		assertThat(config.get(SONAR_RUN), is("true"));
		verify(buildManager, VerificationModeFactory.times(1)).getAllBuildsForEdit();
		verify(build, VerificationModeFactory.times(1)).getBuildDefinition();
		verify(build, VerificationModeFactory.times(0)).getName();
		verify(buildDefinition, VerificationModeFactory.times(1)).getCustomConfiguration();
		verify(buildManager, VerificationModeFactory.times(0)).saveBuild(build);
		
	}

	/**
	 * Test {@link SonarFixSonarServersUpgradeTask#doUpgrade()}
	 * 
	 * @throws Exception in case of errors
	 */
	@Test
	public void testDoUpgradeWithGlobalServer() throws Exception {
		final BambooSonarServer server = new BambooSonarServer();
		server.setHost("http://localhost:9000");
		server.setAdditionalArguments("-Pclover");
		server.setDatabaseUrl("embedded.database");
		server.setDatabaseDriver("com.mysql.driver");
		server.setDatabaseUsername("sonar");
		server.setDatabasePassword("sonar");
		when(bandanaManager.getValue(PlanAwareBandanaContext.GLOBAL_CONTEXT, BandanaUtils.SONAR_SERVER_LIST_KEY))
			.thenReturn(Collections.singletonMap(1, server));
		final Map<String, String> config = new HashMap<String, String>();
		config.put(SONAR_RUN, "true");
		config.put(SONAR_SERVER, "1");
		config.put(SONAR_ADD_ARGS, "-Dkey=value -Pclover");
		config.put(SONAR_HOST_URL, "http://localhost:9000");
		config.put(SONAR_JDBC_URL, "embedded.database");
		config.put(SONAR_JDBC_DRIVER, "com.mysql.driver");
		config.put(SONAR_JDBC_USERNAME, "sonar");
		config.put(SONAR_JDBC_PASSWORD, "sonar");
		when(buildManager.getAllBuildsForEdit()).thenReturn(Collections.singletonList(build));
		when(build.getBuildDefinition()).thenReturn(buildDefinition);
		when(build.getName()).thenReturn("Test Build");
		when(buildDefinition.getCustomConfiguration()).thenReturn(config);
		task.doUpgrade();
		assertThat(config.get(SONAR_RUN), is("true"));
		assertThat(config.get(SONAR_SERVER), is("1"));
		assertThat(config.get(SONAR_ADD_ARGS), is("-Dkey=value"));
		assertThat(config.get(SONAR_HOST_URL), is(""));
		assertThat(config.get(SONAR_JDBC_URL), is(""));
		assertThat(config.get(SONAR_JDBC_DRIVER), is(""));
		assertThat(config.get(SONAR_JDBC_USERNAME), is(""));
		assertThat(config.get(SONAR_JDBC_PASSWORD), is(""));
		verify(buildManager, VerificationModeFactory.times(1)).getAllBuildsForEdit();
		verify(build, VerificationModeFactory.times(7)).getBuildDefinition();
		verify(build, VerificationModeFactory.times(1)).getName();
		verify(buildDefinition, VerificationModeFactory.times(7)).getCustomConfiguration();
		verify(buildManager, VerificationModeFactory.times(1)).saveBuild(build);
		verify(bandanaManager, VerificationModeFactory.times(1)).getValue(PlanAwareBandanaContext.GLOBAL_CONTEXT,
			BandanaUtils.SONAR_SERVER_LIST_KEY);
	}

	/**
	 * Test {@link SonarFixSonarServersUpgradeTask#doUpgrade()}
	 * 
	 * @throws Exception in case of errors
	 */
	@Test
	public void testDoUpgradeInvalidGlobalServer() throws Exception {
		final Map<String, String> config = new HashMap<String, String>();
		config.put(SONAR_RUN, "true");
		config.put(SONAR_SERVER, "-1");
		when(buildManager.getAllBuildsForEdit()).thenReturn(Collections.singletonList(build));
		when(build.getBuildDefinition()).thenReturn(buildDefinition);
		when(build.getName()).thenReturn("Test Build");
		when(buildDefinition.getCustomConfiguration()).thenReturn(config);
		task.doUpgrade();
		assertThat(config.get(SONAR_RUN), is("false"));
		verify(buildManager, VerificationModeFactory.times(1)).getAllBuildsForEdit();
		verify(build, VerificationModeFactory.times(2)).getBuildDefinition();
		verify(build, VerificationModeFactory.times(2)).getName();
		verify(buildDefinition, VerificationModeFactory.times(2)).getCustomConfiguration();
		verify(buildManager, VerificationModeFactory.times(1)).saveBuild(build);
	}

	/**
	 * Test {@link SonarFixSonarServersUpgradeTask#doUpgrade()}
	 * 
	 * @throws Exception in case of errors
	 */
	@Test
	public void testDoUpgradeNumberFormatException() throws Exception {
		final Map<String, String> config = new HashMap<String, String>();
		config.put(SONAR_RUN, "true");
		config.put(SONAR_SERVER, "nan");
		when(buildManager.getAllBuildsForEdit()).thenReturn(Collections.singletonList(build));
		when(build.getBuildDefinition()).thenReturn(buildDefinition);
		when(build.getName()).thenReturn("Test Build");
		when(buildDefinition.getCustomConfiguration()).thenReturn(config);
		task.doUpgrade();
		assertThat(config.get(SONAR_RUN), is("false"));
		verify(buildManager, VerificationModeFactory.times(1)).getAllBuildsForEdit();
		verify(build, VerificationModeFactory.times(2)).getBuildDefinition();
		verify(build, VerificationModeFactory.times(2)).getName();
		verify(buildDefinition, VerificationModeFactory.times(2)).getCustomConfiguration();
		verify(buildManager, VerificationModeFactory.times(1)).saveBuild(build);
	}

	/**
	 * Test {@link SonarFixSonarServersUpgradeTask#getBuildNumber()}
	 */
	@Test
	public void testGetBuildNumber() {
		assertThat(task.getBuildNumber(), is(1));
	}

	/**
	 * Test {@link SonarFixSonarServersUpgradeTask#getPluginKey()}
	 */
	@Test
	public void testGetPluginKey() {
		assertThat(task.getPluginKey(), is("com.marvelution.bamboo.plugins.sonar.gadgets"));
	}

	/**
	 * Test {@link SonarFixSonarServersUpgradeTask#getShortDescription()}
	 */
	@Test
	public void testGetShortDescription() {
		assertThat(task.getShortDescription(), is("Upgrade task to cleanup the build plan configurations"));
	}

}
