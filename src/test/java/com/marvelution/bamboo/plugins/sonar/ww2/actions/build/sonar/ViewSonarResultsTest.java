/*
 * Licensed to Marvelution under one or more contributor license 
 * agreements.  See the NOTICE file distributed with this work 
 * for additional information regarding copyright ownership.
 * Marvelution licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package com.marvelution.bamboo.plugins.sonar.ww2.actions.build.sonar;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import java.util.Collection;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import com.atlassian.sal.api.message.I18nResolver;
import com.marvelution.gadgets.sonar.utils.SonarGadgetsUtils;

/**
 * Testcase for {@link ViewSonarResults}
 * 
 * @author <a href="mailto:markrekveld@marvelution.com">Mark rekveld</a>
 * 
 * @see 2.1.0
 */
public class ViewSonarResultsTest {

	private ViewSonarResults sonarResults;

	@Mock
	private I18nResolver i18nResolver;

	/**
	 * Setup the test variables
	 * 
	 * @throws Exception in case of errors
	 */
	@Before
	public void before() throws Exception {
		MockitoAnnotations.initMocks(this);
		sonarResults = new ViewSonarResults();
		sonarResults.setGadgetsUtils(new SonarGadgetsUtils(i18nResolver));
		when(i18nResolver.getAllTranslationsForPrefix("sonar.gadget", Locale.ENGLISH)).thenAnswer(
			new Answer<Map<String, String>>() {
			public Map<String, String> answer(InvocationOnMock invocation) throws Throwable {
				final Map<String, String> messages = new HashMap<String, String>();
				messages.put("sonar.gadget.violations.title", "Sonar Violations");
				messages.put("sonar.gadget.violations.project.title", "Sonar Violations: {0}");
				messages.put("sonar.gadget.coverage.project.title", "Sonar Coverage: {0}");
				messages.put("sonar.gadget.complexity.title", "Sonar Complexity");
				messages.put("sonar.gadget.complexity.project.title", "Sonar Complexity: {0}");
				messages.put("sonar.gadget.loc.title", "Sonar Lines of Code");
				messages.put("sonar.gadget.loc.project.title", "Sonar Lines of Code: {0}");
				messages.put("sonar.gadget.comments.title", "Sonar Comments");
				messages.put("sonar.gadget.treemap.title", "Sonar Treemap");
				messages.put("sonar.gadget.treemap.project.title", "Sonar Treemap: {0}");
				messages.put("sonar.gadget.totalquality.title", "Sonar Total Quality");
				messages.put("sonar.gadget.technicaldept.title", "Sonar Technical Dept");
				return messages;
			}
		});
	}

	/**
	 * Test {@link ViewSonarResults#getWebFragmentsContextMap()}
	 */
	@Test
	public void testGetGadgetIds() {
		final Collection<String> gadgetIds = sonarResults.getGadgetIds();
		assertThat(gadgetIds.contains("loc"), is(true));
		assertThat(gadgetIds.contains("comments"), is(true));
		assertThat(gadgetIds.contains("coverage"), is(false));
		assertThat(gadgetIds.contains("complexity"), is(true));
		assertThat(gadgetIds.contains("violations"), is(true));
		assertThat(gadgetIds.contains("treemap"), is(true));
		assertThat(gadgetIds.contains("totalquality"), is(true));
		assertThat(gadgetIds.contains("technicaldept"), is(true));
	}
	
}
