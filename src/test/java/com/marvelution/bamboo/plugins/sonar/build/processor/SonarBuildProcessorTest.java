/*
 * Licensed to Marvelution under one or more contributor license 
 * agreements.  See the NOTICE file distributed with this work 
 * for additional information regarding copyright ownership.
 * Marvelution licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package com.marvelution.bamboo.plugins.sonar.build.processor;

import static com.marvelution.bamboo.plugins.sonar.utils.SonarConfigurationHelper.*;
import static com.marvelution.bamboo.plugins.sonar.utils.SonarPluginHelper.*;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.*;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import org.apache.tools.ant.types.Commandline;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.internal.verification.VerificationModeFactory;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import com.atlassian.bamboo.bandana.PlanAwareBandanaContext;
import com.atlassian.bamboo.build.Build;
import com.atlassian.bamboo.build.BuildDefinition;
import com.atlassian.bamboo.build.BuildLoggerManager;
import com.atlassian.bamboo.build.VariableSubstitutionBean;
import com.atlassian.bamboo.build.artifact.ArtifactManager;
import com.atlassian.bamboo.build.fileserver.BuildDirectoryManager;
import com.atlassian.bamboo.build.logger.BuildLogFileAccessorFactory;
import com.atlassian.bamboo.build.logger.BuildLogger;
import com.atlassian.bamboo.builder.AntBuilder;
import com.atlassian.bamboo.builder.BuildState;
import com.atlassian.bamboo.builder.JdkManager;
import com.atlassian.bamboo.builder.Maven2Builder;
import com.atlassian.bamboo.builder.Maven2LogHelper;
import com.atlassian.bamboo.command.CommandException;
import com.atlassian.bamboo.command.CommandExecuteStreamHandler;
import com.atlassian.bamboo.commit.Commit;
import com.atlassian.bamboo.configuration.Jdk;
import com.atlassian.bamboo.logger.ErrorUpdateHandler;
import com.atlassian.bamboo.plan.PlanKeys;
import com.atlassian.bamboo.plan.PlanResultKey;
import com.atlassian.bamboo.repository.NameValuePair;
import com.atlassian.bamboo.utils.error.ErrorCollection;
import com.atlassian.bamboo.v2.build.BuildChanges;
import com.atlassian.bamboo.v2.build.BuildContext;
import com.atlassian.bamboo.v2.build.BuildPlanDefinition;
import com.atlassian.bamboo.v2.build.CurrentBuildResult;
import com.atlassian.bamboo.v2.build.agent.capability.Capability;
import com.atlassian.bamboo.v2.build.agent.capability.CapabilityContextImpl;
import com.atlassian.bamboo.v2.build.agent.capability.CapabilitySetManager;
import com.atlassian.bamboo.v2.build.agent.capability.ReadOnlyCapabilitySet;
import com.atlassian.bamboo.v2.build.agent.capability.Requirement;
import com.atlassian.bamboo.v2.build.agent.capability.RequirementImpl;
import com.atlassian.bamboo.v2.build.agent.capability.RequirementSet;
import com.atlassian.bamboo.v2.build.repository.RepositoryV2;
import com.atlassian.bamboo.v2.build.trigger.ManualBuildTriggerReason;
import com.atlassian.bamboo.ww2.actions.build.admin.create.BuildConfiguration;
import com.atlassian.bandana.BandanaManager;
import com.atlassian.plugin.PluginAccessor;
import com.marvelution.AbstractTestCase;
import com.marvelution.bamboo.plugins.sonar.BandanaUtils;
import com.marvelution.bamboo.plugins.sonar.BambooSonarServer;
import com.marvelution.bamboo.plugins.sonar.capability.utils.CapabilityUtils;
import com.opensymphony.xwork.TextProvider;

/**
 * {@link SonarBuildProcessor} Testcase
 * 
 * @author <a href="mailto:markrekveld@marvelution.com">Mark Rekveld</a>
 */
public class SonarBuildProcessorTest extends AbstractTestCase {

	private static final String TEST_PLAN_KEY = "TEST-TRUNK";

	private SonarBuildProcessor sonarProcessor;

	@Mock
	private JdkManager jdkManager;

	@Mock
	private BuildDirectoryManager buildDirectoryManager;

	@Mock
	private BuildLogFileAccessorFactory buildLogFileAccessorFactory;

	@Mock
	private BuildLoggerManager buildLoggerManager;

	@Mock
	private BuildLogger buildLogger;

	@Mock
	private ErrorUpdateHandler errorUpdateHandler;

	@Mock
	private Maven2LogHelper maven2LogHelper;

	@Mock
	private PluginAccessor pluginAccessor;

	@Mock
	private VariableSubstitutionBean variableSubstitutionBean;

	@Mock
	private BuildContext buildContext;

	@Mock
	private BuildPlanDefinition buildPlanDefinition;

	@Mock
	private BuildDefinition buildDefinition;

	@Mock
	private Build build;

	@Mock
	private ReadOnlyCapabilitySet capabilitySet;

	@Mock
	private CapabilitySetManager capabilitySetManager;

	@Mock
	private Capability capability;

	@Mock
	private RequirementSet requirementSet;

	@Mock
	private Requirement requirement;

	@Mock
	private RepositoryV2 repository;

	@Mock
	private Maven2Builder maven2Builder;

	@Mock
	private CurrentBuildResult buildResult;

	private File repositryDirectory;

	@Mock
	private TextProvider textProvider;

	private Answer<String> textAnswer = new Answer<String>() {

		public String answer(InvocationOnMock invocation) throws Throwable {
			return (String) invocation.getArguments()[0];
		}
		
	};

	@Mock
	private BandanaManager bandanaManager;

	@Mock
	private BuildChanges buildChanges;

	@Mock
	private ArtifactManager artifactManager;

	private PlanResultKey planResultKey = PlanKeys.getPlanResultKey(TEST_PLAN_KEY, 1);

	/**
	 * Setup the test before running it
	 * 
	 * @throws Exception in case of exceptions during the setup
	 */
	@Before
	public void before() throws Exception {
		MockitoAnnotations.initMocks(this);
		new CapabilityContextImpl().setCapabilitySet(capabilitySet);
		sonarProcessor = new SonarBuildProcessor();
		initSonarProcessor();
		when(buildContext.getPlanResultKey()).thenReturn(planResultKey);
		when(buildContext.getBuildPlanDefinition()).thenReturn(buildPlanDefinition);
		when(build.getBuildDefinition()).thenReturn(buildDefinition);
		when(buildPlanDefinition.getBuilderV2()).thenReturn(maven2Builder);
		when(buildPlanDefinition.getRepositoryV2()).thenReturn(repository);
		repositryDirectory = new File(System.getProperty("java.io.tmpdir"), "bamboo-sonar-plugin");
		if (!repositryDirectory.exists()) {
			repositryDirectory.mkdirs();
		}
		when(buildContext.getPlanKey()).thenReturn(TEST_PLAN_KEY);
		when(repository.getSourceCodeDirectory(TEST_PLAN_KEY)).thenReturn(repositryDirectory);
		when(buildLoggerManager.getBuildLogger(planResultKey)).thenReturn(buildLogger);
		when(textProvider.getText(anyString())).thenAnswer(textAnswer);
		when(textProvider.getText(anyString(), anyString())).thenAnswer(textAnswer);
		when(textProvider.getText(anyString(), anyString(), anyString())).thenAnswer(textAnswer);
	}

	/**
	 * Cleanup after the test
	 * 
	 * @throws Exception in case of exceptions during the cleanup
	 */
	@After
	public void after() throws Exception {
		sonarProcessor = null;
		if (repositryDirectory != null) {
			repositryDirectory.delete();
		}
	}

	/**
	 * Test Validation of the {@link BuildConfiguration}
	 * 
	 * @throws Exception in case of test exceptions
	 */
	@Test
	public void testValidateCompleteConfiguration() throws Exception {
		when(capabilitySetManager.getSystemCapabilities(SonarBuildProcessor.MAVEN2_BUILDER_KEY)).thenReturn(
			Collections.singletonList(capability));
		when(capability.getKey()).thenReturn(SonarBuildProcessor.DEFAULT_MAVEN2_BUILDER);
		final BuildConfiguration buildConfiguration =
			new BuildConfiguration(getXmlFromClasspath("build-configurations/valid-complete-config.xml"));
		final ErrorCollection errors = sonarProcessor.validate(buildConfiguration);
		assertThat(errors.getTotalErrors(), is(0));
		verify(capabilitySetManager, VerificationModeFactory.times(1)).getSystemCapabilities(
			SonarBuildProcessor.MAVEN2_BUILDER_KEY);
		verify(capability, VerificationModeFactory.times(1)).getKey();
	}

	/**
	 * Test Validation of the {@link BuildConfiguration}
	 * 
	 * @throws Exception in case of test exceptions
	 */
	@Test
	public void testValidateCompleteNonMavenConfiguration() throws Exception {
		when(capabilitySetManager.getSystemCapabilities(SonarBuildProcessor.MAVEN2_BUILDER_KEY)).thenReturn(
			Collections.singletonList(capability));
		when(capability.getKey()).thenReturn(SonarBuildProcessor.DEFAULT_MAVEN2_BUILDER);
		final BuildConfiguration buildConfiguration =
			new BuildConfiguration(getXmlFromClasspath("build-configurations/valid-complete-non-mvn-config.xml"));
		final ErrorCollection errors = sonarProcessor.validate(buildConfiguration);
		assertThat(errors.getTotalErrors(), is(0));
		verify(capabilitySetManager, VerificationModeFactory.times(1)).getSystemCapabilities(
			SonarBuildProcessor.MAVEN2_BUILDER_KEY);
		verify(capability, VerificationModeFactory.times(1)).getKey();
	}

	/**
	 * Test Validation of the {@link BuildConfiguration}
	 * 
	 * @throws Exception in case of test exceptions
	 */
	@Test
	public void testValidateRunOnlyConfiguration() throws Exception {
		when(capabilitySetManager.getSystemCapabilities(SonarBuildProcessor.MAVEN2_BUILDER_KEY)).thenReturn(
			Collections.singletonList(capability));
		when(capability.getKey()).thenReturn(SonarBuildProcessor.DEFAULT_MAVEN2_BUILDER);
		final BuildConfiguration buildConfiguration =
			new BuildConfiguration(getXmlFromClasspath("build-configurations/valid-runonly-config.xml"));
		final ErrorCollection errors = sonarProcessor.validate(buildConfiguration);
		assertThat(errors.getTotalErrors(), is(0));
		assertThat(errors.getTotalErrors(), is(0));
		verify(capabilitySetManager, VerificationModeFactory.times(1)).getSystemCapabilities(
			SonarBuildProcessor.MAVEN2_BUILDER_KEY);
		verify(capability, VerificationModeFactory.times(1)).getKey();
	}

	/**
	 * Test Validation of the {@link BuildConfiguration}
	 * 
	 * @throws Exception in case of test exceptions
	 */
	@Test
	public void testValidateDontRunConfiguration() throws Exception {
		when(capabilitySetManager.getSystemCapabilities(SonarBuildProcessor.MAVEN2_BUILDER_KEY)).thenReturn(
			Collections.singletonList(capability));
		when(capability.getKey()).thenReturn(SonarBuildProcessor.DEFAULT_MAVEN2_BUILDER);
		final BuildConfiguration buildConfiguration =
			new BuildConfiguration(getXmlFromClasspath("build-configurations/valid-dontrun-config.xml"));
		final ErrorCollection errors = sonarProcessor.validate(buildConfiguration);
		assertThat(errors.getTotalErrors(), is(0));
		assertThat(errors.getTotalErrors(), is(0));
		verify(capabilitySetManager, VerificationModeFactory.times(0)).getSystemCapabilities(
			SonarBuildProcessor.MAVEN2_BUILDER_KEY);
		verify(capability, VerificationModeFactory.times(0)).getKey();
	}

	/**
	 * Test Validation of the {@link BuildConfiguration}
	 * 
	 * @throws Exception in case of test exceptions
	 */
	@Test
	public void testValidateIncompleteNonMavenConfiguration() throws Exception {
		when(capabilitySetManager.getSystemCapabilities(SonarBuildProcessor.MAVEN2_BUILDER_KEY)).thenReturn(
			Collections.singletonList(capability));
		when(capability.getKey()).thenReturn(SonarBuildProcessor.DEFAULT_MAVEN2_BUILDER);
		final BuildConfiguration buildConfiguration =
			new BuildConfiguration(getXmlFromClasspath("build-configurations/invalid-complete-non-mvn-config.xml"));
		final ErrorCollection errors = sonarProcessor.validate(buildConfiguration);
		assertThat(errors.getTotalErrors(), is(7));
		assertThat(errors.getErrors().containsKey(SONAR_HOST_URL), is(true));
		assertThat(errors.getErrors().containsKey(SONAR_LIGHT), is(true));
		assertThat(errors.getErrors().containsKey(SONAR_LIGHT_GROUPID), is(true));
		assertThat(errors.getErrors().containsKey(SONAR_LIGHT_ARTIFACTID), is(true));
		assertThat(errors.getErrors().containsKey(SONAR_LIGHT_NAME), is(true));
		assertThat(errors.getErrors().containsKey(SONAR_LIGHT_SOURCES), is(true));
		assertThat(errors.getErrors().containsKey(SONAR_LIGHT_TARGET), is(true));
		verify(capabilitySetManager, VerificationModeFactory.times(1)).getSystemCapabilities(
			SonarBuildProcessor.MAVEN2_BUILDER_KEY);
		verify(capability, VerificationModeFactory.times(1)).getKey();
	}

	/**
	 * Test Validation of the {@link BuildConfiguration}
	 * 
	 * @throws Exception in case of test exceptions
	 */
	@Test
	public void testValidateIncompleteConfiguration() throws Exception {
		when(capabilitySetManager.getSystemCapabilities(SonarBuildProcessor.MAVEN2_BUILDER_KEY)).thenReturn(
			Collections.singletonList(capability));
		when(capability.getKey()).thenReturn(SonarBuildProcessor.DEFAULT_MAVEN2_BUILDER);
		final BuildConfiguration buildConfiguration =
			new BuildConfiguration(getXmlFromClasspath("build-configurations/invalid-complete-config.xml"));
		final ErrorCollection errors = sonarProcessor.validate(buildConfiguration);
		assertThat(errors.getTotalErrors(), is(1));
		assertThat(errors.getErrors().containsKey(SONAR_HOST_URL), is(true));
		assertThat(errors.getErrors().containsKey(SONAR_LIGHT_GROUPID), is(false));
		assertThat(errors.getErrors().containsKey(SONAR_LIGHT_ARTIFACTID), is(false));
		assertThat(errors.getErrors().containsKey(SONAR_LIGHT_NAME), is(false));
		assertThat(errors.getErrors().containsKey(SONAR_LIGHT_SOURCES), is(false));
		verify(capabilitySetManager, VerificationModeFactory.times(1)).getSystemCapabilities(
			SonarBuildProcessor.MAVEN2_BUILDER_KEY);
		verify(capability, VerificationModeFactory.times(1)).getKey();
	}

	/**
	 * Test Validation of the {@link BuildConfiguration}
	 * 
	 * @throws Exception in case of test exceptions
	 */
	@Test
	public void testValidateIncompleteNoHostConfiguration() throws Exception {
		when(capabilitySetManager.getSystemCapabilities(SonarBuildProcessor.MAVEN2_BUILDER_KEY)).thenReturn(
			Collections.singletonList(capability));
		when(capability.getKey()).thenReturn(SonarBuildProcessor.DEFAULT_MAVEN2_BUILDER);
		final BuildConfiguration buildConfiguration =
			new BuildConfiguration(getXmlFromClasspath("build-configurations/invalid-complete-no-host-config.xml"));
		final ErrorCollection errors = sonarProcessor.validate(buildConfiguration);
		assertThat(errors.getTotalErrors(), is(1));
		assertThat(errors.getErrors().containsKey(SONAR_HOST_URL), is(true));
		assertThat(errors.getErrors().containsKey(SONAR_LIGHT_GROUPID), is(false));
		assertThat(errors.getErrors().containsKey(SONAR_LIGHT_ARTIFACTID), is(false));
		assertThat(errors.getErrors().containsKey(SONAR_LIGHT_NAME), is(false));
		assertThat(errors.getErrors().containsKey(SONAR_LIGHT_SOURCES), is(false));
		verify(capabilitySetManager, VerificationModeFactory.times(1)).getSystemCapabilities(
			SonarBuildProcessor.MAVEN2_BUILDER_KEY);
		verify(capability, VerificationModeFactory.times(1)).getKey();
	}

	/**
	 * Test Validation of the {@link BuildConfiguration}
	 * 
	 * @throws Exception in case of test exceptions
	 */
	@Test
	public void testValidateCompleteServerConfiguration() throws Exception {
		when(capabilitySetManager.getSystemCapabilities(SonarBuildProcessor.MAVEN2_BUILDER_KEY)).thenReturn(
			Collections.singletonList(capability));
		when(capability.getKey()).thenReturn(SonarBuildProcessor.DEFAULT_MAVEN2_BUILDER);
		final BuildConfiguration buildConfiguration =
			new BuildConfiguration(getXmlFromClasspath("build-configurations/valid-server-complete-config.xml"));
		final ErrorCollection errors = sonarProcessor.validate(buildConfiguration);
		assertThat(errors.getTotalErrors(), is(0));
		assertThat(errors.getErrors().containsKey(SONAR_SERVER), is(false));
		assertThat(errors.getErrors().containsKey(SONAR_HOST_URL), is(false));
		assertThat(errors.getErrors().containsKey(SONAR_LIGHT_GROUPID), is(false));
		assertThat(errors.getErrors().containsKey(SONAR_LIGHT_ARTIFACTID), is(false));
		assertThat(errors.getErrors().containsKey(SONAR_LIGHT_NAME), is(false));
		assertThat(errors.getErrors().containsKey(SONAR_LIGHT_SOURCES), is(false));
		verify(capabilitySetManager, VerificationModeFactory.times(1)).getSystemCapabilities(
			SonarBuildProcessor.MAVEN2_BUILDER_KEY);
		verify(capability, VerificationModeFactory.times(1)).getKey();
	}

	/**
	 * Test Validation of the {@link BuildConfiguration}
	 * 
	 * @throws Exception in case of test exceptions
	 */
	@Test
	public void testValidateCompleteInvalidServerIdConfiguration() throws Exception {
		when(capabilitySetManager.getSystemCapabilities(SonarBuildProcessor.MAVEN2_BUILDER_KEY)).thenReturn(
			Collections.singletonList(capability));
		when(capability.getKey()).thenReturn(SonarBuildProcessor.DEFAULT_MAVEN2_BUILDER);
		final BuildConfiguration buildConfiguration =
			new BuildConfiguration(getXmlFromClasspath("build-configurations/invalid-server-complete-config.xml"));
		final ErrorCollection errors = sonarProcessor.validate(buildConfiguration);
		assertThat(errors.getTotalErrors(), is(1));
		assertThat(errors.getErrors().containsKey(SONAR_SERVER), is(true));
		assertThat(errors.getErrors().containsKey(SONAR_HOST_URL), is(false));
		assertThat(errors.getErrors().containsKey(SONAR_LIGHT_GROUPID), is(false));
		assertThat(errors.getErrors().containsKey(SONAR_LIGHT_ARTIFACTID), is(false));
		assertThat(errors.getErrors().containsKey(SONAR_LIGHT_NAME), is(false));
		assertThat(errors.getErrors().containsKey(SONAR_LIGHT_SOURCES), is(false));
		verify(capabilitySetManager, VerificationModeFactory.times(1)).getSystemCapabilities(
			SonarBuildProcessor.MAVEN2_BUILDER_KEY);
		verify(capability, VerificationModeFactory.times(1)).getKey();
	}

	/**
	 * Test Validation of the {@link BuildConfiguration}
	 * 
	 * @throws Exception in case of test exceptions
	 */
	@Test
	public void testValidateCompleteInvalidNoServerOrHostConfiguration() throws Exception {
		when(capabilitySetManager.getSystemCapabilities(SonarBuildProcessor.MAVEN2_BUILDER_KEY)).thenReturn(
			Collections.singletonList(capability));
		when(capability.getKey()).thenReturn(SonarBuildProcessor.DEFAULT_MAVEN2_BUILDER);
		final BuildConfiguration buildConfiguration = new BuildConfiguration(
				getXmlFromClasspath("build-configurations/invalid-no-server-or-host-complete-config.xml"));
		final ErrorCollection errors = sonarProcessor.validate(buildConfiguration);
		assertThat(errors.getTotalErrors(), is(1));
		assertThat(errors.getErrors().containsKey(SONAR_SERVER), is(true));
		assertThat(errors.getErrors().containsKey(SONAR_HOST_URL), is(false));
		assertThat(errors.getErrors().containsKey(SONAR_LIGHT_GROUPID), is(false));
		assertThat(errors.getErrors().containsKey(SONAR_LIGHT_ARTIFACTID), is(false));
		assertThat(errors.getErrors().containsKey(SONAR_LIGHT_NAME), is(false));
		assertThat(errors.getErrors().containsKey(SONAR_LIGHT_SOURCES), is(false));
		verify(capabilitySetManager, VerificationModeFactory.times(1)).getSystemCapabilities(
			SonarBuildProcessor.MAVEN2_BUILDER_KEY);
		verify(capability, VerificationModeFactory.times(1)).getKey();
	}

	/**
	 * Test customising build requirements when Maven2Builder and Jdk are configured
	 * 
	 * @throws Exception in case of test exceptions
	 */
	@Test
	public void testCustomizeBuildRequirementsMaven2BuilderAndJdk() throws Exception {
		when(requirementSet.getSystemRequirements(SonarBuildProcessor.MAVEN2_BUILDER_KEY)).thenReturn(
			Collections.singleton(requirement));
		when(requirementSet.getSystemRequirements(Jdk.CAPABILITY_JDK_TYPE)).thenReturn(
			Collections.singleton(requirement));
		final BuildConfiguration buildConfiguration =
			new BuildConfiguration(getXmlFromClasspath("build-configurations/valid-runonly-config.xml"));
		sonarProcessor.customizeBuildRequirements(buildConfiguration, requirementSet);
		verify(requirementSet, VerificationModeFactory.times(0)).addRequirement(requirement);
		verify(requirementSet, VerificationModeFactory.times(1)).getSystemRequirements(
			SonarBuildProcessor.MAVEN2_BUILDER_KEY);
		verify(requirementSet, VerificationModeFactory.times(1)).getSystemRequirements(Jdk.CAPABILITY_JDK_TYPE);
	}

	/**
	 * Test customising build requirements when Maven2Builder and Jdk are configured
	 * 
	 * @throws Exception in case of test exceptions
	 */
	@Test
	public void testCustomizeBuildRequirementsMaven2BuilderAndOtherJdk() throws Exception {
		when(requirementSet.getSystemRequirements(SonarBuildProcessor.MAVEN2_BUILDER_KEY)).thenReturn(
			Collections.singleton(requirement));
		final BuildConfiguration buildConfiguration =
			new BuildConfiguration(getXmlFromClasspath("build-configurations/valid-runonly-otherjdk-config.xml"));
		sonarProcessor.customizeBuildRequirements(buildConfiguration, requirementSet);
		verify(requirementSet, VerificationModeFactory.times(0)).addRequirement(requirement);
		verify(requirementSet, VerificationModeFactory.times(1)).getSystemRequirements(
			SonarBuildProcessor.MAVEN2_BUILDER_KEY);
		verify(requirementSet, VerificationModeFactory.times(0)).getSystemRequirements(Jdk.CAPABILITY_JDK_TYPE);
	}

	/**
	 * Test customising build requirements when no Maven2Builder is configured
	 * 
	 * @throws Exception in case of test exceptions
	 */
	@Test
	public void testCustomizeBuildRequirementsNoMaven2Builder() throws Exception {
		when(requirementSet.getSystemRequirements(SonarBuildProcessor.MAVEN2_BUILDER_KEY)).thenReturn(
			new HashSet<Requirement>());
		when(requirementSet.getSystemRequirements(Jdk.CAPABILITY_JDK_TYPE)).thenReturn(
			Collections.singleton(requirement));
		final BuildConfiguration buildConfiguration =
			new BuildConfiguration(getXmlFromClasspath("build-configurations/valid-runonly-config.xml"));
		sonarProcessor.customizeBuildRequirements(buildConfiguration, requirementSet);
		verify(requirementSet, VerificationModeFactory.times(1)).addRequirement(
			new RequirementImpl(SonarBuildProcessor.DEFAULT_MAVEN2_BUILDER, true, ".*", true));
		verify(requirementSet, VerificationModeFactory.times(1)).getSystemRequirements(
			SonarBuildProcessor.MAVEN2_BUILDER_KEY);
		verify(requirementSet, VerificationModeFactory.times(1)).getSystemRequirements(Jdk.CAPABILITY_JDK_TYPE);
	}

	/**
	 * Test customising build requirements when no Jdk is configured
	 * 
	 * @throws Exception in case of test exceptions
	 */
	@Test
	public void testCustomizeBuildRequirementsNoJdk() throws Exception {
		when(requirementSet.getSystemRequirements(SonarBuildProcessor.MAVEN2_BUILDER_KEY)).thenReturn(
			Collections.singleton(requirement));
		when(requirementSet.getSystemRequirements(Jdk.CAPABILITY_JDK_TYPE)).thenReturn(
			new HashSet<Requirement>());
		final BuildConfiguration buildConfiguration =
			new BuildConfiguration(getXmlFromClasspath("build-configurations/valid-runonly-config.xml"));
		sonarProcessor.customizeBuildRequirements(buildConfiguration, requirementSet);
		verify(requirementSet, VerificationModeFactory.times(1)).addRequirement(
			new RequirementImpl(CapabilityUtils.DEFAULT_JDK, true, ".*", true));
		verify(requirementSet, VerificationModeFactory.times(1)).getSystemRequirements(
			SonarBuildProcessor.MAVEN2_BUILDER_KEY);
		verify(requirementSet, VerificationModeFactory.times(1)).getSystemRequirements(Jdk.CAPABILITY_JDK_TYPE);
	}

	/**
	 * Test customising build requirements when not running Sonar
	 * 
	 * @throws Exception in case of test exceptions
	 */
	@Test
	public void testCustomizeBuildRequirementsNotRunningSonar() throws Exception {
		final BuildConfiguration buildConfiguration =
			new BuildConfiguration(getXmlFromClasspath("build-configurations/valid-dontrun-config.xml"));
		sonarProcessor.customizeBuildRequirements(buildConfiguration, requirementSet);
		verify(requirementSet, VerificationModeFactory.times(0)).addRequirement(requirement);
		verify(requirementSet, VerificationModeFactory.times(0)).getSystemRequirements(
			SonarBuildProcessor.MAVEN2_BUILDER_KEY);
		verify(requirementSet, VerificationModeFactory.times(0)).getSystemRequirements(Jdk.CAPABILITY_JDK_TYPE);
	}

	/**
	 * Test {@link SonarBuildProcessor#call()} without Sonar being configured
	 * 
	 * @throws Exception in case of test exceptions
	 */
	@Test
	public void testCallNotRunningSonar() throws Exception {
		when(buildPlanDefinition.getCustomConfiguration()).thenAnswer(new Answer<Map<String, String>>() {

			public Map<String, String> answer(InvocationOnMock invocation) throws Throwable {
				final Map<String, String> config = new HashMap<String, String>();
				config.put(SONAR_RUN, "false");
				config.put(SONAR_SKIP_ON_BUILD_FAILURE, "true");
				return config;
			}

		});
		when(buildContext.getBuildResult()).thenReturn(buildResult);
		when(buildResult.getBuildState()).thenReturn(BuildState.SUCCESS);
		sonarProcessor.call();
		verify(buildContext, VerificationModeFactory.times(1)).getPlanKey();
		verify(buildPlanDefinition, VerificationModeFactory.times(1)).getCustomConfiguration();
		verify(buildContext, VerificationModeFactory.times(0)).getBuildResult();
		verify(buildResult, VerificationModeFactory.times(0)).getBuildState();
	}

	/**
	 * Test {@link SonarBuildProcessor#call()} without Sonar being configured
	 * 
	 * @throws Exception in case of test exceptions
	 */
	@Test
	public void testCallSkipSonarOnFailure() throws Exception {
		when(buildPlanDefinition.getCustomConfiguration()).thenAnswer(new Answer<Map<String, String>>() {

			public Map<String, String> answer(InvocationOnMock invocation) throws Throwable {
				final Map<String, String> config = new HashMap<String, String>();
				config.put(SONAR_RUN, "true");
				config.put(SONAR_SKIP_ON_BUILD_FAILURE, "true");
				return config;
			}

		});
		when(buildContext.getBuildResult()).thenReturn(buildResult);
		when(buildResult.getBuildState()).thenReturn(BuildState.FAILED);
		sonarProcessor.call();
		verify(buildContext, VerificationModeFactory.times(3)).getPlanKey();
		verify(buildPlanDefinition, VerificationModeFactory.times(2)).getCustomConfiguration();
		verify(buildContext, VerificationModeFactory.times(1)).getBuildResult();
		verify(buildResult, VerificationModeFactory.times(1)).getBuildState();
	}

	/**
	 * Test {@link SonarBuildProcessor#call()} without Sonar being configured
	 * 
	 * @throws Exception in case of test exceptions
	 */
	@Test
	public void testCallSkipSonarOnManualBuild() throws Exception {
		when(buildPlanDefinition.getCustomConfiguration()).thenAnswer(new Answer<Map<String, String>>() {

			public Map<String, String> answer(InvocationOnMock invocation) throws Throwable {
				final Map<String, String> config = new HashMap<String, String>();
				config.put(SONAR_RUN, "true");
				config.put(SONAR_SKIP_ON_BUILD_FAILURE, "false");
				config.put(SONAR_SKIP_ON_MANUAL_BUILD, "true");
				return config;
			}

		});
		when(buildContext.getBuildResult()).thenReturn(buildResult);
		when(buildResult.getBuildState()).thenReturn(BuildState.SUCCESS);
		when(buildContext.getTriggerReason()).thenReturn(new ManualBuildTriggerReason());
		sonarProcessor.call();
		verify(buildContext, VerificationModeFactory.times(3)).getPlanKey();
		verify(buildPlanDefinition, VerificationModeFactory.times(3)).getCustomConfiguration();
		verify(buildContext, VerificationModeFactory.times(0)).getBuildResult();
		verify(buildResult, VerificationModeFactory.times(0)).getBuildState();
		verify(buildContext, VerificationModeFactory.times(1)).getTriggerReason();
	}

	/**
	 * Test {@link SonarBuildProcessor#call()}
	 * 
	 * @throws Exception in case of test exceptions
	 */
	@Test
	public void testCallSkipSonarOnNoCodeChanges() throws Exception {
		when(buildPlanDefinition.getCustomConfiguration()).thenAnswer(new Answer<Map<String, String>>() {

			public Map<String, String> answer(InvocationOnMock invocation) throws Throwable {
				final Map<String, String> config = new HashMap<String, String>();
				config.put(SONAR_RUN, "true");
				config.put(SONAR_SKIP_ON_BUILD_FAILURE, "false");
				config.put(SONAR_SKIP_ON_MANUAL_BUILD, "false");
				config.put(SONAR_SKIP_ON_NO_CODE_CHANGES, "true");
				return config;
			}

		});
		when(buildContext.getBuildResult()).thenReturn(buildResult);
		when(buildResult.getBuildState()).thenReturn(BuildState.SUCCESS);
		when(buildContext.getTriggerReason()).thenReturn(new ManualBuildTriggerReason());
		when(buildContext.getBuildChanges()).thenReturn(buildChanges);
		final List<Commit> commits = new ArrayList<Commit>();
		when(buildChanges.getChanges()).thenReturn(commits);
		sonarProcessor.call();
		verify(buildContext, VerificationModeFactory.times(3)).getPlanKey();
		verify(buildPlanDefinition, VerificationModeFactory.times(4)).getCustomConfiguration();
		verify(buildContext, VerificationModeFactory.times(0)).getBuildResult();
		verify(buildResult, VerificationModeFactory.times(0)).getBuildState();
		verify(buildContext, VerificationModeFactory.times(0)).getTriggerReason();
		verify(buildContext, VerificationModeFactory.times(1)).getBuildChanges();
		verify(buildChanges, VerificationModeFactory.times(1)).getChanges();
	}

	/**
	 * Test {@link SonarBuildProcessor#call()} with Sonar being configured
	 * 
	 * @throws Exception in case of test exceptions
	 */
	@Test
	public void testCallRunningSonar() throws Exception {
		when(buildPlanDefinition.getCustomConfiguration()).thenAnswer(new Answer<Map<String, String>>() {

			public Map<String, String> answer(InvocationOnMock invocation) throws Throwable {
				final Map<String, String> config = new HashMap<String, String>();
				config.put(SONAR_RUN, "true");
				config.put(SONAR_LIGHT, "false");
				config.put(SONAR_SKIP_ON_BUILD_FAILURE, "true");
				return config;
			}

		});
		when(capabilitySet.getCapability(CapabilityUtils.DEFAULT_JDK)).thenReturn(capability);
		when(capability.getValue()).thenReturn("/path/to/jdk/home");
		when(maven2Builder.getBuildJdk()).thenReturn(CapabilityUtils.DEFAULT_JDK_LABEL);
		when(maven2Builder.executeCommand(eq(buildContext), any(CommandExecuteStreamHandler.class),
				eq("MAVEN_OPTS=\"-Xmx256m -XX:MaxPermSize=128m\""), eq("/path/to/jdk/home"), eq(capabilitySet)))
			.thenReturn(0);
		when(maven2Builder.getBuildLoggerManager()).thenReturn(buildLoggerManager);
		when(maven2Builder.getSubstitutedCommandLine(eq(buildContext), any(BuildLogger.class), eq(capabilitySet)))
			.thenAnswer(new Answer<Commandline>() {

			public Commandline answer(InvocationOnMock invocation) throws Throwable {
				final Commandline command = new Commandline();
				command.setExecutable("mvn");
				command.addArguments(new String[] { "-B sonar:sonar" });
				return command;
			}

		});
		when(maven2Builder.getWorkingDirectory()).thenReturn(new File("/bamboo/xml-data/build-dir/TESTPLAN"));
		when(buildLogger.addBuildLogEntry(anyString())).thenAnswer(new Answer<String>() {

			public String answer(InvocationOnMock invocation) throws Throwable {
				return (String) invocation.getArguments()[0];
			}

		});
		when(buildContext.getBuildResult()).thenReturn(buildResult);
		when(buildResult.getBuildState()).thenReturn(BuildState.SUCCESS);
		sonarProcessor = new SonarBuildProcessor() {

			protected Maven2Builder getMaven2Builder(BuildContext context) {
				return maven2Builder;
			}

		};
		initSonarProcessor();
		sonarProcessor.call();
		verify(buildContext, VerificationModeFactory.times(4)).getPlanKey();
		verify(buildPlanDefinition, VerificationModeFactory.times(6)).getCustomConfiguration();
		verify(capabilitySet, VerificationModeFactory.times(1)).getCapability(CapabilityUtils.DEFAULT_JDK);
		verify(capability, VerificationModeFactory.times(1)).getValue();
		verify(maven2Builder, VerificationModeFactory.times(1)).getBuildJdk();
		verify(maven2Builder, VerificationModeFactory.times(1)).executeCommand(eq(buildContext),
			any(CommandExecuteStreamHandler.class), eq("MAVEN_OPTS=\"-Xmx256m -XX:MaxPermSize=128m\""),
			eq("/path/to/jdk/home"), eq(capabilitySet));
		verify(buildLogger, VerificationModeFactory.times(1)).addBuildLogEntry(
			"Executing Sonar analysis for Build Plan 'TEST-TRUNK'.");
		verify(buildContext, VerificationModeFactory.times(3)).getBuildResult();
		verify(buildResult, VerificationModeFactory.times(1)).getBuildState();
	}

	/**
	 * Test {@link SonarBuildProcessor#call()} with Sonar being configured
	 * 
	 * @throws Exception in case of test exceptions
	 */
	@Test
	public void testCallRunningSonarWithMavenOpts() throws Exception {
		when(buildPlanDefinition.getCustomConfiguration()).thenAnswer(new Answer<Map<String, String>>() {

			public Map<String, String> answer(InvocationOnMock invocation) throws Throwable {
				final Map<String, String> config = new HashMap<String, String>();
				config.put(SONAR_RUN, "true");
				config.put(SONAR_LIGHT, "false");
				config.put(SONAR_SKIP_ON_BUILD_FAILURE, "true");
				config.put(SONAR_MAVEN_OPTS, "-Xmx128m -XX:MaxPermSize=64m");
				return config;
			}

		});
		when(capabilitySet.getCapability(CapabilityUtils.DEFAULT_JDK)).thenReturn(capability);
		when(capability.getValue()).thenReturn("/path/to/jdk/home");
		when(maven2Builder.getBuildJdk()).thenReturn(CapabilityUtils.DEFAULT_JDK_LABEL);
		when(maven2Builder.executeCommand(eq(buildContext), any(CommandExecuteStreamHandler.class),
				eq("MAVEN_OPTS=\"-Xmx128m -XX:MaxPermSize=64m\""), eq("/path/to/jdk/home"), eq(capabilitySet)))
			.thenReturn(0);
		when(maven2Builder.getBuildLoggerManager()).thenReturn(buildLoggerManager);
		when(maven2Builder.getSubstitutedCommandLine(eq(buildContext), any(BuildLogger.class), eq(capabilitySet)))
			.thenAnswer(new Answer<Commandline>() {
	
			public Commandline answer(InvocationOnMock invocation) throws Throwable {
				final Commandline command = new Commandline();
				command.setExecutable("mvn");
				command.addArguments(new String[] { "-B sonar:sonar" });
				return command;
			}
	
		});
		when(maven2Builder.getWorkingDirectory()).thenReturn(new File("/bamboo/xml-data/build-dir/TESTPLAN"));
		when(buildLogger.addBuildLogEntry(anyString())).thenAnswer(new Answer<String>() {

			public String answer(InvocationOnMock invocation) throws Throwable {
				return (String) invocation.getArguments()[0];
			}

		});
		when(buildContext.getBuildResult()).thenReturn(buildResult);
		when(buildResult.getBuildState()).thenReturn(BuildState.SUCCESS);
		sonarProcessor = new SonarBuildProcessor() {

			protected Maven2Builder getMaven2Builder(BuildContext context) {
				return maven2Builder;
			}

		};
		initSonarProcessor();
		sonarProcessor.call();
		verify(buildContext, VerificationModeFactory.times(4)).getPlanKey();
		verify(buildPlanDefinition, VerificationModeFactory.times(7)).getCustomConfiguration();
		verify(capabilitySet, VerificationModeFactory.times(1)).getCapability(CapabilityUtils.DEFAULT_JDK);
		verify(capability, VerificationModeFactory.times(1)).getValue();
		verify(maven2Builder, VerificationModeFactory.times(1)).getBuildJdk();
		verify(maven2Builder, VerificationModeFactory.times(1)).executeCommand(eq(buildContext),
			any(CommandExecuteStreamHandler.class), eq("MAVEN_OPTS=\"-Xmx128m -XX:MaxPermSize=64m\""),
			eq("/path/to/jdk/home"), eq(capabilitySet));
		verify(buildLogger, VerificationModeFactory.times(1)).addBuildLogEntry(
			"Executing Sonar analysis for Build Plan 'TEST-TRUNK'.");
		verify(buildContext, VerificationModeFactory.times(3)).getBuildResult();
		verify(buildResult, VerificationModeFactory.times(1)).getBuildState();
	}

	/**
	 * Test {@link SonarBuildProcessor#call()} with Sonar being configured
	 * 
	 * @throws Exception in case of test exceptions
	 */
	@Test
	public void testCallSonarFailBehavior() throws Exception {
		when(buildPlanDefinition.getCustomConfiguration()).thenAnswer(new Answer<Map<String, String>>() {

			public Map<String, String> answer(InvocationOnMock invocation) throws Throwable {
				final Map<String, String> config = new HashMap<String, String>();
				config.put(SONAR_RUN, "true");
				config.put(SONAR_LIGHT, "false");
				config.put(SONAR_SKIP_ON_BUILD_FAILURE, "true");
				config.put(SONAR_FAILURE_BEHAVIOR, SONAR_FAIL_BEHAVIOR);
				return config;
			}

		});
		when(capabilitySet.getCapability(CapabilityUtils.DEFAULT_JDK)).thenReturn(capability);
		when(capability.getValue()).thenReturn("/path/to/jdk/home");
		when(maven2Builder.getBuildJdk()).thenReturn(CapabilityUtils.DEFAULT_JDK_LABEL);
		when(maven2Builder.executeCommand(eq(buildContext), any(CommandExecuteStreamHandler.class),
				eq("MAVEN_OPTS=\"-Xmx256m -XX:MaxPermSize=128m\""), eq("/path/to/jdk/home"), eq(capabilitySet)))
			.thenReturn(1);
		when(maven2Builder.getBuildLoggerManager()).thenReturn(buildLoggerManager);
		when(maven2Builder.getSubstitutedCommandLine(eq(buildContext), any(BuildLogger.class), eq(capabilitySet)))
			.thenAnswer(new Answer<Commandline>() {
	
			public Commandline answer(InvocationOnMock invocation) throws Throwable {
				final Commandline command = new Commandline();
				command.setExecutable("mvn");
				command.addArguments(
					new String[]{ "-B -Dsonar.jdbc.username=sonar -Dsonar.jdbc.password=sonar sonar:sonar" }
				);
				return command;
			}
	
		});
		when(maven2Builder.getWorkingDirectory()).thenReturn(new File("/bamboo/xml-data/build-dir/TESTPLAN"));
		when(buildLogger.addBuildLogEntry(anyString())).thenAnswer(new Answer<String>() {

			public String answer(InvocationOnMock invocation) throws Throwable {
				return (String) invocation.getArguments()[0];
			}

		});
		when(buildContext.getBuildResult()).thenReturn(buildResult);
		when(buildResult.getBuildState()).thenReturn(BuildState.SUCCESS);
		sonarProcessor = new SonarBuildProcessor() {

			protected Maven2Builder getMaven2Builder(BuildContext context) {
				return maven2Builder;
			}

		};
		initSonarProcessor();
		sonarProcessor.call();
		verify(buildContext, VerificationModeFactory.times(4)).getPlanKey();
		verify(buildPlanDefinition, VerificationModeFactory.times(8)).getCustomConfiguration();
		verify(capabilitySet, VerificationModeFactory.times(1)).getCapability(CapabilityUtils.DEFAULT_JDK);
		verify(capability, VerificationModeFactory.times(1)).getValue();
		verify(maven2Builder, VerificationModeFactory.times(1)).getBuildJdk();
		verify(maven2Builder, VerificationModeFactory.times(1)).executeCommand(eq(buildContext),
			any(CommandExecuteStreamHandler.class), eq("MAVEN_OPTS=\"-Xmx256m -XX:MaxPermSize=128m\""),
			eq("/path/to/jdk/home"), eq(capabilitySet));
		verify(buildLogger, VerificationModeFactory.times(1)).addBuildLogEntry(
			"Executing Sonar analysis for Build Plan 'TEST-TRUNK'.");
		verify(buildLogger, VerificationModeFactory.times(1)).addBuildLogEntry(
			"Consult Sonar analysis log '" + SONAR_ARTIFACT_LABEL + "' for analysis details.");
		verify(buildContext, VerificationModeFactory.times(4)).getBuildResult();
		verify(buildResult, VerificationModeFactory.times(1)).getBuildState();
		verify(buildResult, VerificationModeFactory.times(1)).setBuildState(BuildState.FAILED);
	}

	/**
	 * Test {@link SonarBuildProcessor#call()} with Sonar being configured but with the Maven Builder failing
	 * 
	 * @throws Exception in case of test exceptions
	 */
	@Test
	public void testCallRunningSonarExecutionFailure() throws Exception {
		when(buildPlanDefinition.getCustomConfiguration()).thenAnswer(new Answer<Map<String, String>>() {

			public Map<String, String> answer(InvocationOnMock invocation) throws Throwable {
				final Map<String, String> config = new HashMap<String, String>();
				config.put(SONAR_RUN, "true");
				config.put(SONAR_LIGHT, "false");
				config.put(SONAR_SKIP_ON_BUILD_FAILURE, "true");
				return config;
			}

		});
		when(capabilitySet.getCapability(CapabilityUtils.DEFAULT_JDK)).thenReturn(capability);
		when(capability.getValue()).thenReturn("/path/to/jdk/home");
		when(maven2Builder.getBuildJdk()).thenReturn(CapabilityUtils.DEFAULT_JDK_LABEL);
		when(maven2Builder.executeCommand(eq(buildContext), any(CommandExecuteStreamHandler.class),
			eq("MAVEN_OPTS=\"-Xmx256m -XX:MaxPermSize=128m\""), eq("/path/to/jdk/home"), eq(capabilitySet)))
			.thenThrow(new CommandException("Failure"));
		when(maven2Builder.getBuildLoggerManager()).thenReturn(buildLoggerManager);
		when(maven2Builder.getSubstitutedCommandLine(eq(buildContext), any(BuildLogger.class), eq(capabilitySet)))
			.thenAnswer(new Answer<Commandline>() {
	
			public Commandline answer(InvocationOnMock invocation) throws Throwable {
				final Commandline command = new Commandline();
				command.setExecutable("mvn");
				command.addArguments(
					new String[]{ "-B -Dsonar.jdbc.username=sonar -Dsonar.jdbc.password=sonar sonar:sonar" }
				);
				return command;
			}
	
		});
		when(maven2Builder.getWorkingDirectory()).thenReturn(new File("/bamboo/xml-data/build-dir/TESTPLAN"));
		when(buildLogger.addBuildLogEntry(anyString())).thenAnswer(new Answer<String>() {

			public String answer(InvocationOnMock invocation) throws Throwable {
				return (String) invocation.getArguments()[0];
			}

		});
		when(buildContext.getBuildResult()).thenReturn(buildResult);
		when(buildResult.getBuildState()).thenReturn(BuildState.SUCCESS);
		sonarProcessor = new SonarBuildProcessor() {

			protected Maven2Builder getMaven2Builder(BuildContext context) {
				return maven2Builder;
			}

		};
		initSonarProcessor();
		sonarProcessor.call();
		verify(buildContext, VerificationModeFactory.times(4)).getPlanKey();
		verify(buildPlanDefinition, VerificationModeFactory.times(7)).getCustomConfiguration();
		verify(capabilitySet, VerificationModeFactory.times(1)).getCapability(CapabilityUtils.DEFAULT_JDK);
		verify(capability, VerificationModeFactory.times(1)).getValue();
		verify(maven2Builder, VerificationModeFactory.times(1)).getBuildJdk();
		verify(maven2Builder, VerificationModeFactory.times(1)).executeCommand(eq(buildContext),
			any(CommandExecuteStreamHandler.class), eq("MAVEN_OPTS=\"-Xmx256m -XX:MaxPermSize=128m\""),
			eq("/path/to/jdk/home"), eq(capabilitySet));
		verify(buildLogger, VerificationModeFactory.times(1)).addErrorLogEntry(
			"Failed to execute Sonar analysis update for Build Plan 'TEST-TRUNK'.");
		verify(buildLogger, VerificationModeFactory.times(1)).addBuildLogEntry(
			"Executing Sonar analysis for Build Plan 'TEST-TRUNK'.");
		verify(buildContext, VerificationModeFactory.times(3)).getBuildResult();
		verify(buildResult, VerificationModeFactory.times(1)).getBuildState();
	}

	/**
	 * Test {@link SonarBuildProcessor#call()} with Sonar Light being configured
	 * 
	 * @throws Exception in case of test exceptions
	 */
	@Test
	public void testCallRunningSonarLight() throws Exception {
		when(buildPlanDefinition.getCustomConfiguration()).thenAnswer(new Answer<Map<String, String>>() {

			public Map<String, String> answer(InvocationOnMock invocation) throws Throwable {
				final Map<String, String> config = new HashMap<String, String>();
				config.put(SONAR_RUN, "true");
				config.put(SONAR_LIGHT, "true");
				config.put(SONAR_LIGHT_GROUPID, "com.marvelution.bambo.sonar.plugin");
				config.put(SONAR_LIGHT_ARTIFACTID, "sonar-test-case");
				config.put(SONAR_LIGHT_NAME, "Test case");
				config.put(SONAR_LIGHT_SOURCES, "src/java");
				config.put(SONAR_SKIP_ON_BUILD_FAILURE, "true");
				return config;
			}

		});
		when(capabilitySet.getCapability(CapabilityUtils.DEFAULT_JDK)).thenReturn(capability);
		when(capability.getValue()).thenReturn("/path/to/jdk/home");
		when(maven2Builder.getBuildJdk()).thenReturn(CapabilityUtils.DEFAULT_JDK_LABEL);
		when(maven2Builder.executeCommand(eq(buildContext), any(CommandExecuteStreamHandler.class),
			eq("MAVEN_OPTS=\"-Xmx256m -XX:MaxPermSize=128m\""), eq("/path/to/jdk/home"), eq(capabilitySet)))
			.thenReturn(0);
		when(maven2Builder.getBuildLoggerManager()).thenReturn(buildLoggerManager);
		when(maven2Builder.getSubstitutedCommandLine(eq(buildContext), any(BuildLogger.class), eq(capabilitySet)))
			.thenAnswer(new Answer<Commandline>() {
	
			public Commandline answer(InvocationOnMock invocation) throws Throwable {
				final Commandline command = new Commandline();
				command.setExecutable("mvn");
				command.addArguments(new String[] { "-B sonar:sonar" });
				return command;
			}
	
		});
		when(maven2Builder.getWorkingDirectory()).thenReturn(new File("/bamboo/xml-data/build-dir/TESTPLAN"));
		when(buildLogger.addBuildLogEntry(anyString())).thenAnswer(new Answer<String>() {

			public String answer(InvocationOnMock invocation) throws Throwable {
				return (String) invocation.getArguments()[0];
			}

		});
		when(buildContext.getBuildResult()).thenReturn(buildResult);
		when(buildResult.getBuildState()).thenReturn(BuildState.SUCCESS);
		sonarProcessor = new SonarBuildProcessor() {

			protected Maven2Builder getMaven2Builder(BuildContext context) {
				return maven2Builder;
			}

		};
		initSonarProcessor();
		sonarProcessor.call();
		verify(buildContext, VerificationModeFactory.times(5)).getPlanKey();
		verify(buildContext, VerificationModeFactory.times(17)).getBuildPlanDefinition();
		verify(buildPlanDefinition, VerificationModeFactory.times(4)).getRepositoryV2();
		verify(repository, VerificationModeFactory.times(4)).getSourceCodeDirectory(TEST_PLAN_KEY);
		verify(buildPlanDefinition, VerificationModeFactory.times(7)).getCustomConfiguration();
		verify(capabilitySet, VerificationModeFactory.times(1)).getCapability(CapabilityUtils.DEFAULT_JDK);
		verify(capability, VerificationModeFactory.times(1)).getValue();
		verify(maven2Builder, VerificationModeFactory.times(1)).getBuildJdk();
		verify(maven2Builder, VerificationModeFactory.times(1)).executeCommand(eq(buildContext),
			any(CommandExecuteStreamHandler.class), eq("MAVEN_OPTS=\"-Xmx256m -XX:MaxPermSize=128m\""),
			eq("/path/to/jdk/home"), eq(capabilitySet));
		verify(buildLogger, VerificationModeFactory.times(1)).addBuildLogEntry(
			"Executing Sonar analysis for Build Plan 'TEST-TRUNK'.");
		verify(buildContext, VerificationModeFactory.times(3)).getBuildResult();
		verify(buildResult, VerificationModeFactory.times(1)).getBuildState();
	}

	/**
	 * Test getting the {@link Maven2Builder} from a {@link BuildContext} configured with a {@link Maven2Builder}
	 * 
	 * @throws Exception in case of test exceptions
	 */
	@Test
	public void testGetMaven2BuilderFromBuildContext() throws Exception {
		when(buildPlanDefinition.getBuilderV2()).thenReturn(maven2Builder);
		when(buildPlanDefinition.getCustomConfiguration()).thenReturn(
			Collections.singletonMap(SONAR_BUILD_JDK, SONAR_BUILD_JDK_DEFAULT));
		when(maven2Builder.getKey()).thenReturn("mvn2");
		when(maven2Builder.getLabel()).thenReturn("Maven 2.2.1");
		when(maven2Builder.getBuildJdk()).thenReturn("JDK 1.6");
		when(maven2Builder.getBuildDir()).thenReturn(repositryDirectory);
		when(maven2Builder.getGoal()).thenReturn("clean install");
		final Maven2Builder newBuilder = sonarProcessor.getMaven2Builder(buildContext);
		assertThat(newBuilder.getKey(), is(SonarBuildProcessor.MAVEN2_KEY));
		assertThat(newBuilder.getLabel(), is("Maven 2.2.1"));
		assertThat(newBuilder.getBuildJdk(), is("JDK 1.6"));
		assertThat(newBuilder.getBuildDir(), is(repositryDirectory));
		verify(buildPlanDefinition, VerificationModeFactory.times(2)).getBuilderV2();
		verify(maven2Builder, VerificationModeFactory.times(1)).getKey();
		verify(maven2Builder, VerificationModeFactory.times(1)).getLabel();
		verify(maven2Builder, VerificationModeFactory.times(1)).getBuildJdk();
		verify(maven2Builder, VerificationModeFactory.times(1)).getBuildDir();
		verify(buildPlanDefinition, VerificationModeFactory.times(0)).getRepositoryV2();
		verify(buildPlanDefinition, VerificationModeFactory.times(15)).getCustomConfiguration();
	}

	/**
	 * Test getting the {@link Maven2Builder} from a {@link BuildContext} configured with a {@link Maven2Builder}
	 * 
	 * @throws Exception in case of test exceptions
	 */
	@Test
	public void testGetMaven2BuilderFromBuildContextOtherJdk() throws Exception {
		when(buildPlanDefinition.getBuilderV2()).thenReturn(maven2Builder);
		when(buildPlanDefinition.getCustomConfiguration()).thenReturn(
			Collections.singletonMap(SONAR_BUILD_JDK, CapabilityUtils.DEFAULT_JDK_LABEL));
		when(maven2Builder.getKey()).thenReturn("mvn2");
		when(maven2Builder.getLabel()).thenReturn("Maven 2.2.1");
		when(maven2Builder.getBuildJdk()).thenReturn("JDK 1.6");
		when(maven2Builder.getBuildDir()).thenReturn(repositryDirectory);
		when(maven2Builder.getGoal()).thenReturn("clean install");
		final Maven2Builder newBuilder = sonarProcessor.getMaven2Builder(buildContext);
		assertThat(newBuilder.getKey(), is(SonarBuildProcessor.MAVEN2_KEY));
		assertThat(newBuilder.getLabel(), is("Maven 2.2.1"));
		assertThat(newBuilder.getBuildJdk(), is("JDK"));
		assertThat(newBuilder.getBuildDir(), is(repositryDirectory));
		verify(buildPlanDefinition, VerificationModeFactory.times(2)).getBuilderV2();
		verify(maven2Builder, VerificationModeFactory.times(1)).getKey();
		verify(maven2Builder, VerificationModeFactory.times(1)).getLabel();
		verify(maven2Builder, VerificationModeFactory.times(1)).getBuildJdk();
		verify(maven2Builder, VerificationModeFactory.times(1)).getBuildDir();
		verify(buildPlanDefinition, VerificationModeFactory.times(0)).getRepositoryV2();
		verify(buildPlanDefinition, VerificationModeFactory.times(15)).getCustomConfiguration();
	}

	/**
	 * Test getting the {@link Maven2Builder} from a {@link BuildContext} configured with a {@link Maven2Builder}
	 * 
	 * @throws Exception in case of test exceptions
	 */
	@Test
	public void testGetMaven2BuilderFromBuildContextWithAlternatePomFile() throws Exception {
		when(buildPlanDefinition.getBuilderV2()).thenReturn(maven2Builder);
		when(buildPlanDefinition.getCustomConfiguration()).thenReturn(
			Collections.singletonMap(SONAR_BUILD_JDK, SONAR_BUILD_JDK_DEFAULT));
		when(maven2Builder.getKey()).thenReturn("mvn2");
		when(maven2Builder.getLabel()).thenReturn("Maven 2.2.1");
		when(maven2Builder.getBuildJdk()).thenReturn("JDK 1.6");
		when(maven2Builder.getBuildDir()).thenReturn(repositryDirectory);
		when(maven2Builder.getGoal()).thenReturn("clean install --file alternate.xml");
		final Maven2Builder newBuilder = sonarProcessor.getMaven2Builder(buildContext);
		assertThat(newBuilder.getKey(), is(SonarBuildProcessor.MAVEN2_KEY));
		assertThat(newBuilder.getLabel(), is("Maven 2.2.1"));
		assertThat(newBuilder.getBuildJdk(), is("JDK 1.6"));
		assertThat(newBuilder.getBuildDir(), is(repositryDirectory));
		assertThat(newBuilder.getProjectFile(), is("alternate.xml"));
		verify(buildPlanDefinition, VerificationModeFactory.times(2)).getBuilderV2();
		verify(maven2Builder, VerificationModeFactory.times(1)).getKey();
		verify(maven2Builder, VerificationModeFactory.times(1)).getLabel();
		verify(maven2Builder, VerificationModeFactory.times(1)).getBuildJdk();
		verify(maven2Builder, VerificationModeFactory.times(1)).getBuildDir();
		verify(buildPlanDefinition, VerificationModeFactory.times(0)).getRepositoryV2();
		verify(buildPlanDefinition, VerificationModeFactory.times(15)).getCustomConfiguration();
	}

	/**
	 * Test getting the {@link Maven2Builder} from a {@link BuildContext} configured with a {@link Maven2Builder}
	 * 
	 * @throws Exception in case of test exceptions
	 */
	@Test
	public void testGetMaven2BuilderFromBuildContextWithAlternatePomFileFromProjectFile() throws Exception {
		when(buildPlanDefinition.getBuilderV2()).thenReturn(maven2Builder);
		when(buildPlanDefinition.getCustomConfiguration()).thenReturn(
			Collections.singletonMap(SONAR_BUILD_JDK, SONAR_BUILD_JDK_DEFAULT));
		when(maven2Builder.getProjectFile()).thenReturn("path/to/pom.xml");
		when(maven2Builder.getKey()).thenReturn("mvn2");
		when(maven2Builder.getLabel()).thenReturn("Maven 2.2.1");
		when(maven2Builder.getBuildJdk()).thenReturn("JDK 1.6");
		when(maven2Builder.getBuildDir()).thenReturn(repositryDirectory);
		when(maven2Builder.getGoal()).thenReturn("clean install --file alternate.xml");
		final Maven2Builder newBuilder = sonarProcessor.getMaven2Builder(buildContext);
		assertThat(newBuilder.getKey(), is(SonarBuildProcessor.MAVEN2_KEY));
		assertThat(newBuilder.getLabel(), is("Maven 2.2.1"));
		assertThat(newBuilder.getBuildJdk(), is("JDK 1.6"));
		assertThat(newBuilder.getBuildDir(), is(repositryDirectory));
		assertThat(newBuilder.getProjectFile(), is("path/to/pom.xml"));
		verify(buildPlanDefinition, VerificationModeFactory.times(2)).getBuilderV2();
		verify(maven2Builder, VerificationModeFactory.times(1)).getKey();
		verify(maven2Builder, VerificationModeFactory.times(1)).getLabel();
		verify(maven2Builder, VerificationModeFactory.times(1)).getBuildJdk();
		verify(maven2Builder, VerificationModeFactory.times(1)).getBuildDir();
		verify(buildPlanDefinition, VerificationModeFactory.times(0)).getRepositoryV2();
		verify(buildPlanDefinition, VerificationModeFactory.times(15)).getCustomConfiguration();
	}

	/**
	 * Test getting the {@link Maven2Builder} from a {@link BuildContext} configured with a {@link AntBuilder}
	 * 
	 * @throws Exception in case of test exceptions
	 */
	@Test
	public void testGetMaven2BuilderFromAntBuildContext() throws Exception {
		when(buildPlanDefinition.getBuilderV2()).thenReturn(new AntBuilder());
		when(buildPlanDefinition.getCustomConfiguration()).thenReturn(
			Collections.singletonMap(SONAR_BUILD_JDK, SONAR_BUILD_JDK_DEFAULT));
		final Maven2Builder newBuilder = sonarProcessor.getMaven2Builder(buildContext);
		assertThat(newBuilder.getKey(), is(SonarBuildProcessor.MAVEN2_KEY));
		assertThat(newBuilder.getLabel(), is(SonarBuildProcessor.DEFAULT_MAVEN2_LABEL));
		assertThat(newBuilder.getBuildJdk(), is(CapabilityUtils.DEFAULT_JDK_LABEL));
		assertThat(newBuilder.getBuildDir(), is(repositryDirectory));
		verify(buildPlanDefinition, VerificationModeFactory.times(3)).getBuilderV2();
		verify(buildPlanDefinition, VerificationModeFactory.times(1)).getRepositoryV2();
		verify(buildPlanDefinition, VerificationModeFactory.times(16)).getCustomConfiguration();
	}

	/**
	 * Test {@link SonarBuildProcessor#populateContextForView(Map, Build)} with a shared server
	 * 
	 * @throws Exception in case of test exceptions
	 */
	@Test
	public void testPopulateContextForViewWithGlobalServer() throws Exception {
		final BambooSonarServer server = new BambooSonarServer();
		server.setServerId(1);
		server.setName("I'm a Sonar Server");
		final Map<String, String> config = new HashMap<String, String>();
		config.put(SONAR_SERVER, "1");
		config.put(SONAR_RUN, "true");
		when(buildDefinition.getCustomConfiguration()).thenReturn(config);
		when(bandanaManager.getValue(PlanAwareBandanaContext.GLOBAL_CONTEXT, BandanaUtils.SONAR_SERVER_LIST_KEY))
			.thenReturn(Collections.singletonMap(1, server));
		final Map<String, Object> context = new HashMap<String, Object>();
		sonarProcessor.populateContextForView(context, build);
		assertThat(context.containsKey("server"), is(true));
		assertThat(((BambooSonarServer) context.get("server")).getName(), is("I'm a Sonar Server"));
		assertThat(((BambooSonarServer) context.get("server")).getServerId(), is(1));
		verify(buildDefinition, VerificationModeFactory.times(3)).getCustomConfiguration();
		verify(bandanaManager, VerificationModeFactory.times(1)).getValue(PlanAwareBandanaContext.GLOBAL_CONTEXT,
			BandanaUtils.SONAR_SERVER_LIST_KEY);
	}

	/**
	 * Test {@link SonarBuildProcessor#populateContextForView(Map, Build)} with a invalid shared server
	 * 
	 * @throws Exception in case of test exceptions
	 */
	@Test
	public void testPopulateContextForViewWithInvalidGlobalServer() throws Exception {
		final Map<String, String> config = new HashMap<String, String>();
		config.put(SONAR_SERVER, "ImNotANumber");
		config.put(SONAR_RUN, "true");
		when(buildDefinition.getCustomConfiguration()).thenReturn(config);
		final Map<String, Object> context = new HashMap<String, Object>();
		sonarProcessor.populateContextForView(context, build);
		assertThat(context.containsKey("server"), is(true));
		assertThat(((BambooSonarServer) context.get("server")).getName(), is("INVALID"));
		verify(buildDefinition, VerificationModeFactory.times(3)).getCustomConfiguration();
		verify(bandanaManager, VerificationModeFactory.times(1)).getValue(PlanAwareBandanaContext.GLOBAL_CONTEXT,
			BandanaUtils.SONAR_SERVER_LIST_KEY);
	}

	/**
	 * Test {@link SonarBuildProcessor#populateContextForView(Map, Build)} with a buildPlanSpecific configuration
	 * 
	 * @throws Exception in case of test exceptions
	 */
	@Test
	public void testPopulateContextForViewBuildPlanSpecificServer() throws Exception {
		final Map<String, String> config = new HashMap<String, String>();
		config.put(SONAR_SERVER, BUILD_PLAN_SPECIFIC);
		when(buildDefinition.getCustomConfiguration()).thenReturn(config);
		final Map<String, Object> context = new HashMap<String, Object>();
		sonarProcessor.populateContextForView(context, build);
		assertThat(context.containsKey("server"), is(false));
		verify(buildDefinition, VerificationModeFactory.times(1)).getCustomConfiguration();
	}

	/**
	 * Test {@link SonarBuildProcessor#populateContextForEdit(Map, BuildConfiguration, Build)}
	 * 
	 * @throws Exception in case of test exceptions
	 */
	@SuppressWarnings("unchecked")
	@Test
	public void testPopulateContextForEdit() throws Exception {
		when(capabilitySetManager.getSystemCapabilities(Jdk.CAPABILITY_JDK_TYPE)).thenReturn(
			Collections.singletonList(capability));
		when(capability.getKey()).thenReturn(Jdk.CAPABILITY_JDK_PREFIX + ".JDK");
		final BuildConfiguration buildConfig = new BuildConfiguration();
		final BambooSonarServer server = new BambooSonarServer();
		server.setServerId(1);
		server.setName("I'm a Sonar Server");
		when(bandanaManager.getValue(PlanAwareBandanaContext.GLOBAL_CONTEXT, BandanaUtils.SONAR_SERVER_LIST_KEY))
			.thenReturn(Collections.singletonMap(1, server));
		final Map<String, Object> context = new HashMap<String, Object>();
		sonarProcessor.populateContextForEdit(context, buildConfig, build);
		assertThat(context.containsKey("servers"), is(true));
		final List<NameValuePair> servers = (List<NameValuePair>) context.get("servers");
		assertThat(servers.get(0).getName(), is("buildPlanSpecific"));
		assertThat(servers.get(0).getLabel(), is("sonar.build.plan.specific.server"));
		assertThat(servers.get(0).getValue(), is("buildPlanSpecific"));
		assertThat(servers.get(1).getName(), is("1"));
		assertThat(servers.get(1).getLabel(), is("I'm a Sonar Server"));
		assertThat(servers.get(1).getValue(), is("1"));
		assertThat(context.containsKey("jdks"), is(true));
		final List<String> jdks = (List<String>) context.get("jdks");
		assertThat(jdks.get(0), is(SONAR_BUILD_JDK_DEFAULT));
		assertThat(jdks.get(1), is("JDK"));
		verify(bandanaManager, VerificationModeFactory.times(1)).getValue(PlanAwareBandanaContext.GLOBAL_CONTEXT,
			BandanaUtils.SONAR_SERVER_LIST_KEY);
		verify(capabilitySetManager, VerificationModeFactory.times(1)).getSystemCapabilities(Jdk.CAPABILITY_JDK_TYPE);
	}

	/**
	 * Init the SonarProcessor by setting all the dependent managers and the buildContext
	 */
	private void initSonarProcessor() {
		sonarProcessor.setBuildDirectoryManager(buildDirectoryManager);
		sonarProcessor.setBuildLogFileAccessorFactory(buildLogFileAccessorFactory);
		sonarProcessor.setBuildLoggerManager(buildLoggerManager);
		sonarProcessor.setErrorUpdateHandler(errorUpdateHandler);
		sonarProcessor.setJdkManager(jdkManager);
		sonarProcessor.setMaven2LogHelper(maven2LogHelper);
		sonarProcessor.setPluginAccessor(pluginAccessor);
		sonarProcessor.setVariableSubstitutionBean(variableSubstitutionBean);
		sonarProcessor.setCapabilitySetManager(capabilitySetManager);
		sonarProcessor.setTextProvider(textProvider);
		sonarProcessor.setBandanaManager(bandanaManager);
		sonarProcessor.setArtifactManager(artifactManager);
		sonarProcessor.init(buildContext);
	}

}
