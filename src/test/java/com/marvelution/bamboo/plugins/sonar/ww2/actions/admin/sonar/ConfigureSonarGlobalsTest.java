/*
 * Licensed to Marvelution under one or more contributor license 
 * agreements.  See the NOTICE file distributed with this work 
 * for additional information regarding copyright ownership.
 * Marvelution licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package com.marvelution.bamboo.plugins.sonar.ww2.actions.admin.sonar;

import static com.marvelution.bamboo.plugins.sonar.utils.SonarConfigurationHelper.*;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.internal.verification.VerificationModeFactory;

import com.atlassian.bamboo.configuration.AdministrationConfiguration;
import com.atlassian.bamboo.configuration.AdministrationConfigurationManager;
import com.atlassian.bamboo.repository.NameValuePair;

/**
 * {@link ConfigureSonarGlobals} testcase
 * 
 * @author <a href="mailto:markrekveld@marvelution.com">Mark Rekveld</a>
 */
public class ConfigureSonarGlobalsTest {

	private ConfigureSonarGlobals configureSonarGlobals;

	@Mock
	private AdministrationConfigurationManager administrationConfigurationManager;

	@Mock
	private AdministrationConfiguration administrationConfiguration;

	/**
	 * Setup the test variables
	 * 
	 * @throws Exception in case of exceptions
	 */
	@Before
	public void before() throws Exception {
		MockitoAnnotations.initMocks(this);
		configureSonarGlobals = new TestConfigureSonarGlobals();
		configureSonarGlobals.setAdministrationConfigurationManager(administrationConfigurationManager);
		when(administrationConfigurationManager.getAdministrationConfiguration()).thenReturn(
			administrationConfiguration);
	}

	/**
	 * Test {@link ConfigureSonarGlobals#doDefault()}
	 * 
	 * @throws Exception in case of test exceptions
	 */
	@Test
	public void testDoDefault() throws Exception {
		when(administrationConfiguration.getSystemProperty(SONAR_SKIP_ON_BUILD_FAILURE)).thenReturn("true");
		when(administrationConfiguration.getSystemProperty(SONAR_SKIP_ON_MANUAL_BUILD)).thenReturn("true");
		when(administrationConfiguration.getSystemProperty(SONAR_SKIP_ON_NO_CODE_CHANGES)).thenReturn("true");
		when(administrationConfiguration.getSystemProperty(SONAR_SKIP_BYTECODE_ANALYSIS)).thenReturn("true");
		when(administrationConfiguration.getSystemProperty(SONAR_PROFILE)).thenReturn("Marvelution");
		assertThat(configureSonarGlobals.doDefault(), is(ConfigureSonarGlobals.INPUT));
		assertThat(configureSonarGlobals.isSkipOnBuildFailure(), is(true));
		assertThat(configureSonarGlobals.isSkipOnManualBuild(), is(true));
		assertThat(configureSonarGlobals.isSkipOnNoCodeChanges(), is(true));
		assertThat(configureSonarGlobals.isSkipByteCodeAnalysis(), is(true));
		assertThat(configureSonarGlobals.getSonarProfile(), is("Marvelution"));
		verify(administrationConfiguration, VerificationModeFactory.times(1)).getSystemProperty(
			SONAR_SKIP_ON_BUILD_FAILURE);
		verify(administrationConfiguration, VerificationModeFactory.times(1)).getSystemProperty(
			SONAR_SKIP_ON_MANUAL_BUILD);
		verify(administrationConfiguration, VerificationModeFactory.times(1)).getSystemProperty(
			SONAR_SKIP_ON_NO_CODE_CHANGES);
		verify(administrationConfiguration, VerificationModeFactory.times(1)).getSystemProperty(
			SONAR_SKIP_BYTECODE_ANALYSIS);
		verify(administrationConfiguration, VerificationModeFactory.times(1)).getSystemProperty(SONAR_PROFILE);
		verify(administrationConfigurationManager, VerificationModeFactory.times(1)).getAdministrationConfiguration();
	}

	/**
	 * Test {@link ConfigureSonarGlobals#execute()}
	 * 
	 * @throws Exception in case of test exceptions
	 */
	@Test
	public void testExecute() throws Exception {
		configureSonarGlobals.setSkipOnBuildFailure(true);
		configureSonarGlobals.setSkipOnManualBuild(true);
		configureSonarGlobals.setSkipOnNoCodeChanges(true);
		configureSonarGlobals.setSkipByteCodeAnalysis(true);
		configureSonarGlobals.setSonarFailureBehavior(SONAR_FAIL_BEHAVIOR);
		configureSonarGlobals.setSonarProfile("Marvelution");
		assertThat(configureSonarGlobals.execute(), is(ConfigureSonarGlobals.SUCCESS));
		assertThat(configureSonarGlobals.getActionMessages().isEmpty(), is(false));
		assertThat(configureSonarGlobals.getActionMessages().contains("sonar.global.config.updated"), is(true));
		verify(administrationConfiguration, VerificationModeFactory.times(1)).setSystemProperty(
			SONAR_SKIP_ON_BUILD_FAILURE, "true");
		verify(administrationConfiguration, VerificationModeFactory.times(1)).setSystemProperty(
			SONAR_SKIP_ON_MANUAL_BUILD, "true");
		verify(administrationConfiguration, VerificationModeFactory.times(1)).setSystemProperty(
			SONAR_SKIP_ON_NO_CODE_CHANGES, "true");
		verify(administrationConfiguration, VerificationModeFactory.times(1)).setSystemProperty(
			SONAR_SKIP_BYTECODE_ANALYSIS, "true");
		verify(administrationConfiguration, VerificationModeFactory.times(1)).setSystemProperty(
			SONAR_FAILURE_BEHAVIOR, SONAR_FAIL_BEHAVIOR);
		verify(administrationConfiguration, VerificationModeFactory.times(1)).setSystemProperty(SONAR_PROFILE,
			"Marvelution");
		verify(administrationConfigurationManager, VerificationModeFactory.times(1)).getAdministrationConfiguration();
		verify(administrationConfigurationManager, VerificationModeFactory.times(1)).saveAdministrationConfiguration(
			administrationConfiguration);
	}

	/**
	 * Validate the Sonar Fail behaviors
	 */
	@Test
	public void testValidateFailBehaviors() {
		final List<NameValuePair> behaviors = configureSonarGlobals.getBehaviors();
		assertThat(behaviors.isEmpty(), is(false));
		assertThat(behaviors.size(), is(3));
		assertThat(behaviors.get(0).getValue(), is(SONAR_IGNORE_BEHAVIOR));
		assertThat(behaviors.get(1).getValue(), is(SONAR_FAIL_BEHAVIOR));
		assertThat(behaviors.get(2).getValue(), is(SONAR_LABEL_BEHAVIOR));
	}

	/**
	 * Test class to override the get text method
	 * 
	 * @author <a href="mailto:markrekveld@marvelution.com">Mark Rekveld</a>
	 */
	public class TestConfigureSonarGlobals extends ConfigureSonarGlobals {

		private static final long serialVersionUID = 1L;

		/**
		 * {@inheritDoc}
		 */
		@Override
		public String getText(String key) {
			return key;
		}

	}

}
