/*
 * Licensed to Marvelution under one or more contributor license 
 * agreements.  See the NOTICE file distributed with this work 
 * for additional information regarding copyright ownership.
 * Marvelution licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package com.marvelution.bamboo.plugins.sonar.ww2.actions.admin.sonar;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.internal.verification.VerificationModeFactory;

import com.atlassian.bamboo.bandana.PlanAwareBandanaContext;
import com.atlassian.bandana.BandanaManager;
import com.atlassian.core.util.map.EasyMap;
import com.marvelution.bamboo.plugins.sonar.BandanaUtils;
import com.marvelution.bamboo.plugins.sonar.BambooSonarServer;

/**
 * Testcase for {@link ConfigureSonarServers}
 * 
 * @author <a href="mailto:markrekveld@marvelution.com">Mark Rekveld</a>
 */
public class ConfigureSonarServersTest {

	private ConfigureSonarServers configureSonarServers;

	@Mock
	private BandanaManager bandanaManager;

	@Mock
	private BambooSonarServer sonarServer;

	/**
	 * Setup the test variables
	 * 
	 * @throws Exception in case of exceptions
	 */
	@Before
	public void before() throws Exception {
		MockitoAnnotations.initMocks(this);
		configureSonarServers = new TestConfigureSonarServers();
		configureSonarServers.setBandanaManager(bandanaManager);
	}

	/**
	 * Test {@link ConfigureSonarServers#doDefault()}
	 * 
	 * @throws Exception in case of test exceptions
	 */
	@Test
	public void testDoDefault() throws Exception {
		when(bandanaManager.getValue(PlanAwareBandanaContext.GLOBAL_CONTEXT, BandanaUtils.SONAR_SERVER_LIST_KEY))
			.thenReturn(Collections.singletonMap(1, sonarServer));
		assertThat(configureSonarServers.doDefault(), is(ConfigureSonarServers.INPUT));
		assertThat(configureSonarServers.getSonarServers().isEmpty(), is(false));
		assertThat(configureSonarServers.getSonarServers().size(), is(1));
		verify(bandanaManager, VerificationModeFactory.times(1)).getValue(PlanAwareBandanaContext.GLOBAL_CONTEXT,
			BandanaUtils.SONAR_SERVER_LIST_KEY);
	}

	/**
	 * Test {@link ConfigureSonarServers#doAdd()}
	 * 
	 * @throws Exception in case of test exceptions
	 */
	@Test
	public void testDoAdd() throws Exception {
		assertThat(configureSonarServers.doAdd(), is(ConfigureSonarServers.INPUT));
		assertThat(configureSonarServers.getMode(), is(ConfigureSonarServers.ADD));
	}

	/**
	 * Test {@link ConfigureSonarServers#doCreate()}
	 * 
	 * @throws Exception in case of test exceptions
	 */
	@Test
	public void testDoCreate() throws Exception {
		when(bandanaManager.getValue(PlanAwareBandanaContext.GLOBAL_CONTEXT, BandanaUtils.NEXT_SONAR_SERVER_ID_KEY))
			.thenReturn(2);
		when(bandanaManager.getValue(PlanAwareBandanaContext.GLOBAL_CONTEXT, BandanaUtils.SONAR_SERVER_LIST_KEY))
			.thenReturn(EasyMap.build(1, sonarServer));
		when(sonarServer.getName()).thenReturn("Sonar");
		configureSonarServers.setServerName("Sonar Server");
		configureSonarServers.setServerDescription("I'm a Sonar Test Server");
		configureSonarServers.setServerHost("http://localhost:9000/");
		configureSonarServers.setServerUsername("user");
		configureSonarServers.setServerPassword("user");
		configureSonarServers.setServerDisabled(false);
		configureSonarServers.setDatabaseUrl("jndi:jdbc:mysql");
		configureSonarServers.setDatabaseDriver("com.mysql.jdbc.Driver");
		configureSonarServers.setDatabaseUsername("sonar");
		configureSonarServers.setDatabasePassword("sonar");
		configureSonarServers.setAdditionalArguments("-Dprop.name=prop.vale");
		assertThat(configureSonarServers.doCreate(), is(ConfigureSonarServers.SUCCESS));
		assertThat(configureSonarServers.getActionErrors().isEmpty(), is(true));
		assertThat(configureSonarServers.getActionMessages().isEmpty(), is(false));
		assertThat(configureSonarServers.getActionMessages().contains("sonar.global.messages.server.action"),
			is(true));
		verify(bandanaManager, VerificationModeFactory.times(1)).getValue(PlanAwareBandanaContext.GLOBAL_CONTEXT,
			BandanaUtils.SONAR_SERVER_LIST_KEY);
		verify(bandanaManager, VerificationModeFactory.times(1)).setValue(eq(PlanAwareBandanaContext.GLOBAL_CONTEXT),
			eq(BandanaUtils.SONAR_SERVER_LIST_KEY), any(Map.class));
		verify(bandanaManager, VerificationModeFactory.times(1)).getValue(PlanAwareBandanaContext.GLOBAL_CONTEXT,
			BandanaUtils.NEXT_SONAR_SERVER_ID_KEY);
		verify(bandanaManager, VerificationModeFactory.times(1)).setValue(PlanAwareBandanaContext.GLOBAL_CONTEXT,
			BandanaUtils.NEXT_SONAR_SERVER_ID_KEY, 3);
	}

	/**
	 * Test {@link ConfigureSonarServers#doEdit()} valid serverId
	 * 
	 * @throws Exception in case of test exceptions
	 */
	@Test
	public void testDoEditValidServerId() throws Exception {
		final BambooSonarServer server = new BambooSonarServer();
		server.setServerId(1);
		server.setName("Sonar Server");
		server.setDescription("A Test Sonar Server");
		server.setUsername("user");
		server.setPassword("user");
		server.setDisabled(false);
		server.setHost("http://localhost:9000/");
		server.setDatabaseUrl("jndi:jdbc:mysql:somewhere");
		server.setDatabaseDriver("com.mysql.jdbc.Driver");
		server.setDatabaseUsername("sonar");
		server.setDatabasePassword("sonar");
		server.setAdditionalArguments("-Dprop.name=prop.value");
		when(bandanaManager.getValue(PlanAwareBandanaContext.GLOBAL_CONTEXT, BandanaUtils.SONAR_SERVER_LIST_KEY))
			.thenReturn(Collections.singletonMap(1, server));
		configureSonarServers.setServerId(1);
		assertThat(configureSonarServers.doEdit(), is(ConfigureSonarServers.INPUT));
		assertThat(configureSonarServers.getMode(), is(ConfigureSonarServers.EDIT));
		assertThat(configureSonarServers.getActionErrors().isEmpty(), is(true));
		assertThat(configureSonarServers.getServerName(), is("Sonar Server"));
		assertThat(configureSonarServers.getServerUsername(), is("user"));
		assertThat(configureSonarServers.getServerPassword(), is("user"));
		assertThat(configureSonarServers.getServerDescription(), is("A Test Sonar Server"));
		assertThat(configureSonarServers.isServerDisabled(), is(false));
		assertThat(configureSonarServers.getServerHost(), is("http://localhost:9000"));
		assertThat(configureSonarServers.getDatabaseUrl(), is("jndi:jdbc:mysql:somewhere"));
		assertThat(configureSonarServers.getDatabaseDriver(), is("com.mysql.jdbc.Driver"));
		assertThat(configureSonarServers.getDatabaseUsername(), is("sonar"));
		assertThat(configureSonarServers.getDatabasePassword(), is("sonar"));
		assertThat(configureSonarServers.getAdditionalArguments(), is("-Dprop.name=prop.value"));
		verify(bandanaManager, VerificationModeFactory.times(1)).getValue(PlanAwareBandanaContext.GLOBAL_CONTEXT,
			BandanaUtils.SONAR_SERVER_LIST_KEY);
	}

	/**
	 * Test {@link ConfigureSonarServers#doEdit()} invalid serverId
	 * 
	 * @throws Exception in case of test exceptions
	 */
	@Test
	public void testDoEditInvalidServerId() throws Exception {
		assertThat(configureSonarServers.doEdit(), is(ConfigureSonarServers.INPUT));
		assertThat(configureSonarServers.getMode(), is(ConfigureSonarServers.EDIT));
		assertThat(configureSonarServers.getActionErrors().isEmpty(), is(false));
		assertThat(configureSonarServers.getActionErrors().contains("sonar.global.errors.no.server.id"), is(true));
		verify(bandanaManager, VerificationModeFactory.times(1)).getValue(PlanAwareBandanaContext.GLOBAL_CONTEXT,
			BandanaUtils.SONAR_SERVER_LIST_KEY);
	}

	/**
	 * Test {@link ConfigureSonarServers#doUpdate()}
	 * 
	 * @throws Exception in case of test exceptions
	 */
	@Test
	public void testDoUpdate() throws Exception {
		configureSonarServers.setServerId(1);
		configureSonarServers.setServerName("Sonar Server");
		configureSonarServers.setServerDescription("I'm a Sonar Test Server");
		configureSonarServers.setServerHost("http://localhost:9000/");
		configureSonarServers.setServerDisabled(false);
		configureSonarServers.setDatabaseUrl("jndi:jdbc:mysql");
		configureSonarServers.setDatabaseDriver("com.mysql.jdbc.Driver");
		configureSonarServers.setDatabaseUsername("sonar");
		configureSonarServers.setDatabasePassword("sonar");
		configureSonarServers.setAdditionalArguments("-Dprop.name=prop.vale");
		assertThat(configureSonarServers.doUpdate(), is(ConfigureSonarServers.SUCCESS));
		assertThat(configureSonarServers.getActionErrors().isEmpty(), is(true));
		assertThat(configureSonarServers.getActionMessages().isEmpty(), is(false));
		assertThat(configureSonarServers.getActionMessages().contains("sonar.global.messages.server.action"),
			is(true));
		verify(bandanaManager, VerificationModeFactory.times(1)).getValue(PlanAwareBandanaContext.GLOBAL_CONTEXT,
			BandanaUtils.SONAR_SERVER_LIST_KEY);
		verify(bandanaManager, VerificationModeFactory.times(1)).setValue(eq(PlanAwareBandanaContext.GLOBAL_CONTEXT),
			eq(BandanaUtils.SONAR_SERVER_LIST_KEY), any(Map.class));
		verify(bandanaManager, VerificationModeFactory.times(0)).getValue(PlanAwareBandanaContext.GLOBAL_CONTEXT,
			BandanaUtils.NEXT_SONAR_SERVER_ID_KEY);
		verify(bandanaManager, VerificationModeFactory.times(0)).setValue(PlanAwareBandanaContext.GLOBAL_CONTEXT,
			BandanaUtils.NEXT_SONAR_SERVER_ID_KEY, 2);
	}

	/**
	 * Test {@link ConfigureSonarServers#doDelete()}
	 * 
	 * @throws Exception in case of test exceptions
	 */
	@Test
	public void testDoDelete() throws Exception {
		final Map<Integer, BambooSonarServer> servers = new HashMap<Integer, BambooSonarServer>();
		servers.put(1, sonarServer);
		when(bandanaManager.getValue(PlanAwareBandanaContext.GLOBAL_CONTEXT, BandanaUtils.SONAR_SERVER_LIST_KEY))
			.thenReturn(servers);
		configureSonarServers.setServerId(1);
		assertThat(configureSonarServers.doDelete(), is(ConfigureSonarServers.SUCCESS));
		assertThat(configureSonarServers.getActionErrors().isEmpty(), is(true));
		assertThat(configureSonarServers.getActionMessages().isEmpty(), is(false));
		assertThat(configureSonarServers.getActionMessages().contains("sonar.global.messages.server.action"), is(true));
		verify(bandanaManager, VerificationModeFactory.times(1)).getValue(PlanAwareBandanaContext.GLOBAL_CONTEXT,
			BandanaUtils.SONAR_SERVER_LIST_KEY);
		verify(bandanaManager, VerificationModeFactory.times(1)).setValue(PlanAwareBandanaContext.GLOBAL_CONTEXT,
			BandanaUtils.SONAR_SERVER_LIST_KEY, servers);
	}

	/**
	 * Test {@link ConfigureSonarServers#doDelete()} with an invalid serverid
	 * 
	 * @throws Exception in case of test exceptions
	 */
	@Test
	public void testDoDeleteInvalidServerId() throws Exception {
		assertThat(configureSonarServers.doDelete(), is(ConfigureSonarServers.ERROR));
		assertThat(configureSonarServers.getActionErrors().isEmpty(), is(false));
		assertThat(configureSonarServers.getActionErrors().contains("sonar.global.errors.no.server.id"), is(true));
		verify(bandanaManager, VerificationModeFactory.times(1)).getValue(PlanAwareBandanaContext.GLOBAL_CONTEXT,
			BandanaUtils.SONAR_SERVER_LIST_KEY);
	}

	/**
	 * Test {@link ConfigureSonarServers#doEnable()}
	 * 
	 * @throws Exception in case of test exceptions
	 */
	@Test
	public void testDoEnable() throws Exception {
		final Map<Integer, BambooSonarServer> servers = new HashMap<Integer, BambooSonarServer>();
		servers.put(1, sonarServer);
		when(bandanaManager.getValue(PlanAwareBandanaContext.GLOBAL_CONTEXT, BandanaUtils.SONAR_SERVER_LIST_KEY))
			.thenReturn(servers);
		configureSonarServers.setServerId(1);
		assertThat(configureSonarServers.doEnable(), is(ConfigureSonarServers.SUCCESS));
		assertThat(configureSonarServers.getActionErrors().isEmpty(), is(true));
		assertThat(configureSonarServers.getActionMessages().isEmpty(), is(false));
		assertThat(configureSonarServers.getActionMessages().contains("sonar.global.messages.server.action"), is(true));
		verify(bandanaManager, VerificationModeFactory.times(1)).getValue(PlanAwareBandanaContext.GLOBAL_CONTEXT,
			BandanaUtils.SONAR_SERVER_LIST_KEY);
		verify(bandanaManager, VerificationModeFactory.times(1)).setValue(PlanAwareBandanaContext.GLOBAL_CONTEXT,
			BandanaUtils.SONAR_SERVER_LIST_KEY, servers);
	}

	/**
	 * Test {@link ConfigureSonarServers#doEnable()} with an invalid serverid
	 * 
	 * @throws Exception in case of test exceptions
	 */
	@Test
	public void testDoEnableInvalidServerId() throws Exception {
		assertThat(configureSonarServers.doEnable(), is(ConfigureSonarServers.ERROR));
		assertThat(configureSonarServers.getActionErrors().isEmpty(), is(false));
		assertThat(configureSonarServers.getActionErrors().contains("sonar.global.errors.no.server.id"), is(true));
		verify(bandanaManager, VerificationModeFactory.times(1)).getValue(PlanAwareBandanaContext.GLOBAL_CONTEXT,
			BandanaUtils.SONAR_SERVER_LIST_KEY);
	}

	/**
	 * Test {@link ConfigureSonarServers#doDisable()}
	 * 
	 * @throws Exception in case of test exceptions
	 */
	@Test
	public void testDoDisable() throws Exception {
		final Map<Integer, BambooSonarServer> servers = new HashMap<Integer, BambooSonarServer>();
		servers.put(1, sonarServer);
		when(bandanaManager.getValue(PlanAwareBandanaContext.GLOBAL_CONTEXT, BandanaUtils.SONAR_SERVER_LIST_KEY))
			.thenReturn(servers);
		configureSonarServers.setServerId(1);
		assertThat(configureSonarServers.doDisable(), is(ConfigureSonarServers.SUCCESS));
		assertThat(configureSonarServers.getActionErrors().isEmpty(), is(true));
		assertThat(configureSonarServers.getActionMessages().isEmpty(), is(false));
		assertThat(configureSonarServers.getActionMessages().contains("sonar.global.messages.server.action"), is(true));
		verify(bandanaManager, VerificationModeFactory.times(1)).getValue(PlanAwareBandanaContext.GLOBAL_CONTEXT,
			BandanaUtils.SONAR_SERVER_LIST_KEY);
		verify(bandanaManager, VerificationModeFactory.times(1)).setValue(PlanAwareBandanaContext.GLOBAL_CONTEXT,
			BandanaUtils.SONAR_SERVER_LIST_KEY, servers);
	}

	/**
	 * Test {@link ConfigureSonarServers#doDisable()} with an invalid serverid
	 * 
	 * @throws Exception in case of test exceptions
	 */
	@Test
	public void testDoDisableInvalidServerId() throws Exception {
		assertThat(configureSonarServers.doDisable(), is(ConfigureSonarServers.ERROR));
		assertThat(configureSonarServers.getActionErrors().isEmpty(), is(false));
		assertThat(configureSonarServers.getActionErrors().contains("sonar.global.errors.no.server.id"), is(true));
		verify(bandanaManager, VerificationModeFactory.times(1)).getValue(PlanAwareBandanaContext.GLOBAL_CONTEXT,
			BandanaUtils.SONAR_SERVER_LIST_KEY);
	}

	/**
	 * Test {@link ConfigureSonarServers#execute()} with duplicate server name error
	 * 
	 * @throws Exception in case of test exceptions
	 */
	@SuppressWarnings("unchecked")
	@Test
	public void testExecuteDuplicateServerName() throws Exception {
		when(bandanaManager.getValue(PlanAwareBandanaContext.GLOBAL_CONTEXT, BandanaUtils.SONAR_SERVER_LIST_KEY))
			.thenReturn(EasyMap.build(1, sonarServer));
		when(sonarServer.getServerId()).thenReturn(1);
		when(sonarServer.getName()).thenReturn("Sonar Server");
		configureSonarServers.setServerName("Sonar Server");
		configureSonarServers.setServerHost("http://localhost:9000/");
		configureSonarServers.setServerDisabled(false);
		assertThat(configureSonarServers.doCreate(), is(ConfigureSonarServers.ERROR));
		assertThat(configureSonarServers.getFieldErrors().isEmpty(), is(false));
		assertThat(configureSonarServers.getFieldErrors().containsKey("serverName"), is(true));
		assertThat(((List<String>) configureSonarServers.getFieldErrors().get("serverName"))
			.contains("sonar.server.name.no.duplicates"), is(true));
		assertThat(configureSonarServers.getActionErrors().isEmpty(), is(true));
		assertThat(configureSonarServers.getActionMessages().isEmpty(), is(true));
		verify(bandanaManager, VerificationModeFactory.times(1)).getValue(PlanAwareBandanaContext.GLOBAL_CONTEXT,
			BandanaUtils.SONAR_SERVER_LIST_KEY);
		verify(bandanaManager, VerificationModeFactory.times(0)).setValue(eq(PlanAwareBandanaContext.GLOBAL_CONTEXT),
			eq(BandanaUtils.SONAR_SERVER_LIST_KEY), any(Map.class));
		verify(bandanaManager, VerificationModeFactory.times(0)).getValue(PlanAwareBandanaContext.GLOBAL_CONTEXT,
			BandanaUtils.NEXT_SONAR_SERVER_ID_KEY);
		verify(bandanaManager, VerificationModeFactory.times(0)).setValue(PlanAwareBandanaContext.GLOBAL_CONTEXT,
			BandanaUtils.NEXT_SONAR_SERVER_ID_KEY, 2);
	}

	/**
	 * Test {@link ConfigureSonarServers#execute()} with invalid name error
	 * 
	 * @throws Exception in case of test exceptions
	 */
	@SuppressWarnings("unchecked")
	@Test
	public void testExecuteInvalidName() throws Exception {
		configureSonarServers.setServerName("");
		configureSonarServers.setServerHost("http://localhost:9000/");
		configureSonarServers.setServerDisabled(false);
		assertThat(configureSonarServers.doCreate(), is(ConfigureSonarServers.ERROR));
		assertThat(configureSonarServers.getFieldErrors().isEmpty(), is(false));
		assertThat(configureSonarServers.getFieldErrors().containsKey("serverName"), is(true));
		assertThat(((List<String>) configureSonarServers.getFieldErrors().get("serverName"))
			.contains("sonar.server.name.required"), is(true));
		assertThat(configureSonarServers.getActionErrors().isEmpty(), is(true));
		assertThat(configureSonarServers.getActionMessages().isEmpty(), is(true));
		verify(bandanaManager, VerificationModeFactory.times(1)).getValue(PlanAwareBandanaContext.GLOBAL_CONTEXT,
			BandanaUtils.SONAR_SERVER_LIST_KEY);
		verify(bandanaManager, VerificationModeFactory.times(0)).setValue(eq(PlanAwareBandanaContext.GLOBAL_CONTEXT),
			eq(BandanaUtils.SONAR_SERVER_LIST_KEY), any(Map.class));
		verify(bandanaManager, VerificationModeFactory.times(0)).getValue(PlanAwareBandanaContext.GLOBAL_CONTEXT,
			BandanaUtils.NEXT_SONAR_SERVER_ID_KEY);
		verify(bandanaManager, VerificationModeFactory.times(0)).setValue(PlanAwareBandanaContext.GLOBAL_CONTEXT,
			BandanaUtils.NEXT_SONAR_SERVER_ID_KEY, 2);
	}

	/**
	 * Test {@link ConfigureSonarServers#execute()} with empty host error
	 * 
	 * @throws Exception in case of test exceptions
	 */
	@SuppressWarnings("unchecked")
	@Test
	public void testExecuteEmptyHost() throws Exception {
		configureSonarServers.setServerName("Sonar Server");
		configureSonarServers.setServerHost("");
		configureSonarServers.setServerDisabled(false);
		assertThat(configureSonarServers.doCreate(), is(ConfigureSonarServers.ERROR));
		assertThat(configureSonarServers.getFieldErrors().isEmpty(), is(false));
		assertThat(configureSonarServers.getFieldErrors().containsKey("serverHost"), is(true));
		assertThat(((List<String>) configureSonarServers.getFieldErrors().get("serverHost"))
			.contains("sonar.host.url.required"), is(true));
		assertThat(configureSonarServers.getActionErrors().isEmpty(), is(true));
		assertThat(configureSonarServers.getActionMessages().isEmpty(), is(true));
		verify(bandanaManager, VerificationModeFactory.times(1)).getValue(PlanAwareBandanaContext.GLOBAL_CONTEXT,
			BandanaUtils.SONAR_SERVER_LIST_KEY);
		verify(bandanaManager, VerificationModeFactory.times(0)).setValue(eq(PlanAwareBandanaContext.GLOBAL_CONTEXT),
			eq(BandanaUtils.SONAR_SERVER_LIST_KEY), any(Map.class));
		verify(bandanaManager, VerificationModeFactory.times(0)).getValue(PlanAwareBandanaContext.GLOBAL_CONTEXT,
			BandanaUtils.NEXT_SONAR_SERVER_ID_KEY);
		verify(bandanaManager, VerificationModeFactory.times(0)).setValue(PlanAwareBandanaContext.GLOBAL_CONTEXT,
			BandanaUtils.NEXT_SONAR_SERVER_ID_KEY, 2);
	}

	/**
	 * Test {@link ConfigureSonarServers#execute()} with invalid host error
	 * 
	 * @throws Exception in case of test exceptions
	 */
	@SuppressWarnings("unchecked")
	@Test
	public void testExecuteInvalidHost() throws Exception {
		configureSonarServers.setServerName("Sonar Server");
		configureSonarServers.setServerHost("localhost:9000/");
		configureSonarServers.setServerDisabled(false);
		assertThat(configureSonarServers.doCreate(), is(ConfigureSonarServers.ERROR));
		assertThat(configureSonarServers.getFieldErrors().isEmpty(), is(false));
		assertThat(configureSonarServers.getFieldErrors().containsKey("serverHost"), is(true));
		assertThat(((List<String>) configureSonarServers.getFieldErrors().get("serverHost"))
			.contains("sonar.host.url.invalid"), is(true));
		assertThat(configureSonarServers.getActionErrors().isEmpty(), is(true));
		assertThat(configureSonarServers.getActionMessages().isEmpty(), is(true));
		verify(bandanaManager, VerificationModeFactory.times(1)).getValue(PlanAwareBandanaContext.GLOBAL_CONTEXT,
			BandanaUtils.SONAR_SERVER_LIST_KEY);
		verify(bandanaManager, VerificationModeFactory.times(0)).setValue(eq(PlanAwareBandanaContext.GLOBAL_CONTEXT),
			eq(BandanaUtils.SONAR_SERVER_LIST_KEY), any(Map.class));
		verify(bandanaManager, VerificationModeFactory.times(0)).getValue(PlanAwareBandanaContext.GLOBAL_CONTEXT,
			BandanaUtils.NEXT_SONAR_SERVER_ID_KEY);
		verify(bandanaManager, VerificationModeFactory.times(0)).setValue(PlanAwareBandanaContext.GLOBAL_CONTEXT,
			BandanaUtils.NEXT_SONAR_SERVER_ID_KEY, 2);
	}

	/**
	 * Test class to override the get text method
	 * 
	 * @author <a href="mailto:markrekveld@marvelution.com">Mark Rekveld</a>
	 */
	public class TestConfigureSonarServers extends ConfigureSonarServers {

		private static final long serialVersionUID = 1L;

		/**
		 * {@inheritDoc}
		 */
		@Override
		public String getText(String key) {
			return key;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public String getText(String key, String value) {
			return key;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public String getText(String key, String value, String value1) {
			return key;
		}

	}

}
