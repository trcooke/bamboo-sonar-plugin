/*
 * Licensed to Marvelution under one or more contributor license 
 * agreements.  See the NOTICE file distributed with this work 
 * for additional information regarding copyright ownership.
 * Marvelution licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package com.marvelution.bamboo.plugins.sonar.capability.utils;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.internal.verification.VerificationModeFactory;

import com.atlassian.bamboo.configuration.Jdk;
import com.atlassian.bamboo.v2.build.agent.capability.Capability;
import com.atlassian.bamboo.v2.build.agent.capability.ReadOnlyCapabilitySet;

/**
 * Testcase for {@link CapabilityUtils} helper class
 * 
 * @author <a href="mailto:markrekveld@marvelution.com">Mark Rekveld</a>
 */
public class CapabilityUtilsTest {

	@Mock
	private ReadOnlyCapabilitySet capabilitySet;

	@Mock
	private Capability capability;

	/**
	 * Setup the Mocks for the tests
	 * 
	 * @throws Exception in case of exceptions
	 */
	@Before
	public void before() throws Exception {
		MockitoAnnotations.initMocks(this);
	}

	/**
	 * Test getJdkPath for default JDK
	 */
	@Test
	public void testGetJdkPathDefaultJdk() {
		when(capabilitySet.getCapability(Jdk.CAPABILITY_JDK_PREFIX + "." + CapabilityUtils.DEFAULT_JDK_LABEL))
			.thenReturn(capability);
		when(capability.getValue()).thenReturn("path/to/default/jdk");
		final String jdkPath = CapabilityUtils.getJdkPath(null, capabilitySet);
		assertThat(jdkPath, is("path/to/default/jdk"));
		verify(capabilitySet, VerificationModeFactory.times(1)).getCapability(
			Jdk.CAPABILITY_JDK_PREFIX + "." + CapabilityUtils.DEFAULT_JDK_LABEL);
		verify(capability, VerificationModeFactory.times(1)).getValue();
	}

	/**
	 * Test getJdkPath for default JDK
	 */
	@Test
	public void testGetJdkPathNoCapability() {
		when(capabilitySet.getCapability(Jdk.CAPABILITY_JDK_PREFIX + "." + CapabilityUtils.DEFAULT_JDK_LABEL))
			.thenReturn(null);
		final String jdkPath = CapabilityUtils.getJdkPath(null, capabilitySet);
		assertThat(jdkPath, nullValue());
		verify(capabilitySet, VerificationModeFactory.times(1)).getCapability(
			Jdk.CAPABILITY_JDK_PREFIX + "." + CapabilityUtils.DEFAULT_JDK_LABEL);
		verify(capability, VerificationModeFactory.times(0)).getValue();
	}

	/**
	 * Test getJdkPath for default JDK
	 */
	@Test
	public void testGetJdkPath() {
		when(capabilitySet.getCapability(Jdk.CAPABILITY_JDK_PREFIX + ".JDK 1.6"))
			.thenReturn(capability);
		when(capability.getValue()).thenReturn("path/to/default/jdk");
		final String jdkPath = CapabilityUtils.getJdkPath("JDK 1.6", capabilitySet);
		assertThat(jdkPath, is("path/to/default/jdk"));
		verify(capabilitySet, VerificationModeFactory.times(1)).getCapability(
			Jdk.CAPABILITY_JDK_PREFIX + ".JDK 1.6");
		verify(capability, VerificationModeFactory.times(1)).getValue();
	}

}
