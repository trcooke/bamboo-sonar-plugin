/*
 * Licensed to Marvelution under one or more contributor license 
 * agreements.  See the NOTICE file distributed with this work 
 * for additional information regarding copyright ownership.
 * Marvelution licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package com.marvelution;

import java.io.IOException;
import java.io.InputStream;

import org.codehaus.plexus.util.IOUtil;

/**
 * Abstract Testcase to share common functions
 * 
 * @author <a href="mailto:markrekveld@marvelution.com">Mark Rekveld</a>
 */
public abstract class AbstractTestCase {

	/**
	 * Setup the test variables
	 * 
	 * @throws Exception in case of setup exceptions
	 */
	public abstract void before() throws Exception;

	/**
	 * Cleanup after the test
	 * 
	 * @throws Exception in case of cleanup exceptions
	 */
	public abstract void after() throws Exception;

	/**
	 * Load the XML file from the class-path
	 * 
	 * @param filename the filename to load
	 * @return the content of the file
	 * @throws IOException in case of load exceptions
	 */
	protected String getXmlFromClasspath(String filename) throws IOException {
		final InputStream inputStream = Thread.currentThread().getContextClassLoader().getResourceAsStream(filename);
		return IOUtil.toString(inputStream);
	}

}
