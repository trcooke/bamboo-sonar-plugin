/*
 * Licensed to Marvelution under one or more contributor license 
 * agreements.  See the NOTICE file distributed with this work 
 * for additional information regarding copyright ownership.
 * Marvelution licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

AJS.$.namespace("AJS.sonar.panel");

/**
 * Base object containing all the options for the panel
 */
AJS.sonar.panel.config = {};

/**
 * Initiate the Sonar panel
 * 
 * @param panel the configuration object for the panel
 */
AJS.sonar.panel.initPanel = function(panel) {
	AJS.sonar.panel.config = panel;
	var panelContainer = AJS.$(AJS.sonar.panel.config.panelContainer);
	for (var columnId in AJS.sonar.panel.config.layout) {
		AJS.$('<div/>').attr({id: columnId}).addClass(AJS.sonar.panel.config.columnClass).appendTo(panelContainer);
		AJS.$(AJS.sonar.panel.config.layout[columnId]).each(function(index, gadgetId) {
			AJS.sonar.panel.addGadgetToColumn(columnId, gadgetId);
		});
	}
}

/**
 * Add a gadget to a column in the panel
 * 
 * @param columnId the columnId to add the gadget to
 * @param gadgetId the id of the gadget to add
 */
AJS.sonar.panel.addGadgetToColumn = function(columnId, gadgetId) {
	var gadget = AJS.sonar.panel.createGadgetBox(gadgetId);
	AJS.$('#' + columnId).prepend(gadget);
}

/**
 * Helper method to create the gadget draggable box
 * 
 * @param gadgetId the id of the gadget
 * @return the jQuery wrapped div
 */
AJS.sonar.panel.createGadgetBox = function(gadgetId) {
	var header = AJS.$('<div/>').addClass('dragbox-header').append(
		AJS.$('<h3/>').text(AJS.sonar.panel.config.gadgets[gadgetId].title)
	).attr({gadget: gadgetId});
	return AJS.$('<div/>').attr({id: gadgetId}).addClass('dragbox').append(header).append(
		AJS.$('<div/>').addClass('dragbox-content').append(AJS.$('<iframe/>').attr({
			id: gadgetId + '-gadget-iframe',
			width: '100%',
			src: BASE_URL + "/plugins/servlet/sonar/gadgetServlet?buildPlanKey=" + AJS.sonar.panel.config.buildPlanKey + "&gadgetId=" + gadgetId,
			scrolling: 'no',
			frameborder: 'no'
		}))
	);
}

/**
 * Resize the panel container to make sure all the gadgets can be shown completely
 */
AJS.sonar.resizePanelContainer = function() {
	var height = 0;
	AJS.$(".column").each(function(){
		var column = AJS.$(this);
		if (column.height() > height) {
			height = column.height();
		}
	});
	AJS.$(AJS.sonar.panel.config.panelContainer).height(height);
}

/**
 * Resize the gadget Iframe to fit the gadget content
 * 
 * @param gadgetId the Id of the gadget
 * @param height the height in pixels to set the Iframe to
 */
AJS.sonar.panel.resizeGadget = function(gadgetId, height) {
	AJS.$(AJS.$("#" + gadgetId + "-gadget-iframe").get(0)).height(height);
	// After resizing the gadget, resize the container
	AJS.sonar.resizePanelContainer();
}
