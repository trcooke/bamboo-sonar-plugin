<!--
 ~ Licensed to Marvelution under one or more contributor license 
 ~ agreements.  See the NOTICE file distributed with this work 
 ~ for additional information regarding copyright ownership.
 ~ Marvelution licenses this file to you under the Apache License,
 ~ Version 2.0 (the "License"); you may not use this file except
 ~ in compliance with the License.
 ~ You may obtain a copy of the License at
 ~
 ~  http://www.apache.org/licenses/LICENSE-2.0
 ~
 ~ Unless required by applicable law or agreed to in writing,
 ~ software distributed under the License is distributed on an
 ~ "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 ~ KIND, either express or implied. See the License for the
 ~ specific language governing permissions and limitations
 ~ under the License.
 -->
<html>
<head>
	<title>[@ww.text name='sonar.global.servers.title' /]</title>
	<meta name="decorator" content="adminpage">
</head>
<body>
	<h1>[@ww.text name='sonar.global.servers.heading' /]</h1>
	<p>[@ww.text name='sonar.global.servers.description' /]</p>
	[@ww.actionerror /]
	[@ui.clear/]    
	[@ui.bambooPanel titleKey='sonar.global.servers.list.heading']
		<div class="contentSection">
		[#if sonarServers?has_content]
		<table id="sonar-servers" class="grid" width="100%">
			<colgroup>
				<col>
				<col width="450">
				<col width="185">
			</colgroup>
			<thead><tr>
				<th>[@ww.text name='sonar.global.servers.list.heading.server' /]</th>
				<th>[@ww.text name='sonar.global.servers.list.heading.configuration' /]</th>
				<th>[@ww.text name='sonar.global.servers.list.heading.operations' /]</th>
			</tr></thead>
			<tbody>
				[#foreach server in sonarServers]
					<tr>
						<td>
							<a href="${server.host}">${server.name?html}</a><br />
							[#if server.description?has_content]
								<div class="fieldDescription">${server.description}</div>
							[/#if]
						</td>
						<td>
							[@ww.text name='sonar.global.servers.list.host' /]: <a href="${server.host}">${server.host}</a><br />
							[#if server.databaseUrl?has_content]
								[@ww.text name='sonar.global.servers.list.database' /]: ${server.databaseUrl}<br />
							[#else]
								[@ww.text name='sonar.global.servers.list.database' /]: [@ww.text name='sonar.global.servers.list.database.default' /]<br />
							[/#if]
							[#if server.databaseDriver?has_content]
								[@ww.text name='sonar.global.servers.list.driver' /]: ${server.databaseDriver}<br />
							[#else]
								[@ww.text name='sonar.global.servers.list.driver' /]: [@ww.text name='sonar.global.servers.list.driver.default' /]<br />
							[/#if]
							[#if server.databaseUsername?has_content]
								[@ww.text name='sonar.global.servers.list.username' /]: ${server.databaseUsername}<br />
							[#else]
								[@ww.text name='sonar.global.servers.list.username' /]: [@ww.text name='sonar.global.servers.list.username.default' /]<br />
							[/#if]
							[#if server.additionalArguments?has_content]
								[@ww.text name='sonar.global.servers.list.add.args' /]: ${server.additionalArguments}
							[/#if]
						</td>
						<td>
							<a href="${req.contextPath}/admin/sonar/editSonarServer.action?serverId=${server.serverId}">[@ww.text name='sonar.global.edit.server' /]</a>
							| <a href="${req.contextPath}/admin/sonar/deleteSonarServer.action?serverId=${server.serverId}">[@ww.text name='sonar.global.delete.server' /]</a>
							[#if server.disabled]
							| <a href="${req.contextPath}/admin/sonar/enableSonarServer.action?serverId=${server.serverId}">[@ww.text name='sonar.global.enable.server' /]</a>
							[#else]
							| <a href="${req.contextPath}/admin/sonar/disableSonarServer.action?serverId=${server.serverId}">[@ww.text name='sonar.global.disable.server' /]</a>
							[/#if]
						</td>
					</tr>
				[/#foreach]
			</tbody>
		</table>
		[#else]
			[@ww.text name='sonar.global.servers.none' /]
		[/#if]
		</div>
		<div class="formFooter buttons">
			<a href="[@ww.url action='addSonarServer' namespace='/admin/sonar' /]">[@ww.text name='sonar.global.add.server' /]</a>
		</div>
	[/@ui.bambooPanel]
</body>
</html>