<!--
 ~ Licensed to Marvelution under one or more contributor license 
 ~ agreements.  See the NOTICE file distributed with this work 
 ~ for additional information regarding copyright ownership.
 ~ Marvelution licenses this file to you under the Apache License,
 ~ Version 2.0 (the "License"); you may not use this file except
 ~ in compliance with the License.
 ~ You may obtain a copy of the License at
 ~
 ~  http://www.apache.org/licenses/LICENSE-2.0
 ~
 ~ Unless required by applicable law or agreed to in writing,
 ~ software distributed under the License is distributed on an
 ~ "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 ~ KIND, either express or implied. See the License for the
 ~ specific language governing permissions and limitations
 ~ under the License.
 -->
[#if mode == 'edit' ]
	[#assign targetAction = "/admin/sonar/updateSonarServer.action"]
[#else]
	[#assign targetAction = "/admin/sonar/createSonarServer.action"]
[/#if]
<html>
<head>
	<title>[@ww.text name='sonar.global.${mode}.server.title' /]</title>
	<meta name="decorator" content="adminpage">
</head>
<body>
	<h1>[@ww.text name='sonar.global.${mode}.server.heading' /]</h1>
	<p>[@ww.text name='sonar.global.${mode}.server.description' /]</p>
	[@ww.actionerror /]
	[@ui.clear /]    
	[@ww.form action=targetAction
			submitLabelKey='sonar.global.${mode}.server.button'
			titleKey='sonar.global.${mode}.server.form.title'
			cancelUri='/admin/sonar/viewSonarServers.action'
			showActionErrors=true
			descriptionKey='sonar.global.${mode}.server.form.description']
		[@ww.hidden name='mode' /]
		[@ww.hidden name='serverDisabled' /]
		[#if mode == 'edit']
			[@ww.hidden name='serverId' /]
		[/#if]
		[@ww.textfield name='serverName' labelKey='sonar.server.name' descriptionKey='sonar.server.name.description' required='true' /]
		[@ww.textfield name='serverDescription' labelKey='sonar.server.description' descriptionKey='sonar.server.description.description' /]
		[@ww.textfield name='serverHost' labelKey='sonar.host.url' descriptionKey='sonar.host.url.description' required='true' /]
		[@ww.textfield name='serverUsername' labelKey='sonar.host.username' descriptionKey='sonar.host.username.description' /]
		[@ww.textfield name='serverPassword' labelKey='sonar.host.password' descriptionKey='sonar.host.password.description' /]
		[@ui.bambooSection titleKey='sonar.jdbc.configuration']
			[@ww.textfield name='databaseUrl' labelKey='sonar.jdbc.url' descriptionKey='sonar.jdbc.url.description' /]
			[@ww.textfield name='databaseDriver' labelKey='sonar.jdbc.driver' descriptionKey='sonar.jdbc.driver.description' /]
			[@ww.textfield name='databaseUsername' labelKey='sonar.jdbc.username' descriptionKey='sonar.jdbc.username.description' /]
			[@ww.textfield name='databasePassword' labelKey='sonar.jdbc.password' descriptionKey='sonar.jdbc.password.description' /]
		[/@ui.bambooSection]
		[@ui.bambooSection titleKey='sonar.extra.configuration']
			[@ww.textfield name='additionalArguments' labelKey='sonar.add.args' descriptionKey='sonar.add.args.description' /]
		[/@ui.bambooSection]
	[/@ww.form]
</body>
</html>