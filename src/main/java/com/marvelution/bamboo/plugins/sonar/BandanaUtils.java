/*
 * Licensed to Marvelution under one or more contributor license 
 * agreements.  See the NOTICE file distributed with this work 
 * for additional information regarding copyright ownership.
 * Marvelution licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package com.marvelution.bamboo.plugins.sonar;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import com.atlassian.bamboo.bandana.PlanAwareBandanaContext;
import com.atlassian.bandana.BandanaManager;

/**
 * Utility class for Bandana related storage
 * 
 * @author <a href="mailto:markrekveld@marvelution.com">Mark Rekveld</a>
 */
public class BandanaUtils {

	/**
	 * Bandana key for the {@link BambooSonarServer} list
	 */
	public static final String SONAR_SERVER_LIST_KEY = "com.marvelution.bamboo.plugins.sonar:SonarServersList";

	/**
	 * Bandana key for the next ID for a {@link BambooSonarServer}
	 */
	public static final String NEXT_SONAR_SERVER_ID_KEY = "com.marvelution.bamboo.plugins.sonar:SonarServerNextId";

	private BandanaManager bandanaManager;

	private Map<Integer, BambooSonarServer> sonarServers;

	/**
	 * Constructor
	 */
	public BandanaUtils() {
		sonarServers = new HashMap<Integer, BambooSonarServer>();
	}

	/**
	 * Set the {@link BandanaManager}
	 * 
	 * @param bandanaManager the {@link BandanaManager}
	 */
	public void setBandanaManager(BandanaManager bandanaManager) {
		this.bandanaManager = bandanaManager;
	}

	/**
	 * get a {@link BambooSonarServer} by Id
	 * 
	 * @param serverId the Id of the server
	 * @return the {@link BambooSonarServer}
	 */
	public BambooSonarServer getSonarServer(int serverId) {
		return sonarServers.get(serverId);
	}

	/**
	 * Add a {@link BambooSonarServer} to the servers {@link Map}
	 * 
	 * @param server the {@link BambooSonarServer} to add
	 */
	public void addUpdateSonarServer(BambooSonarServer server) {
		if (server.getServerId() < 1) {
			server.setServerId(getNextSonarServerId());
		}
		sonarServers.put(server.getServerId(), server);
		storeSonarServers();
	}

	/**
	 * Remove a {@link BambooSonarServer} from the servers {@link Map}
	 * 
	 * @param server the {@link BambooSonarServer} to remove
	 */
	public void removeSonarServer(BambooSonarServer server) {
		sonarServers.remove(server.getServerId());
		storeSonarServers();
	}

	/**
	 * Disable a {@link BambooSonarServer}
	 * 
	 * @param server the {@link BambooSonarServer} to disable
	 */
	public void disableSonarServer(BambooSonarServer server) {
		server.setDisabled(true);
		sonarServers.put(server.getServerId(), server);
		storeSonarServers();
	}

	/**
	 * Enable a {@link BambooSonarServer}
	 * 
	 * @param server the {@link BambooSonarServer} to Enable
	 */
	public void enableSonarServer(BambooSonarServer server) {
		server.setDisabled(false);
		sonarServers.put(server.getServerId(), server);
		storeSonarServers();
	}

	/**
	 * Load the {@link BambooSonarServer} {@link Map} using the {@link BandanaManager}
	 */
	@SuppressWarnings("unchecked")
	public void loadSonarServers() {
		if (bandanaManager == null) {
			throw new IllegalStateException("BandanaManager is not set");
		}
		final Object bandanaValue = bandanaManager.getValue(PlanAwareBandanaContext.GLOBAL_CONTEXT,
			SONAR_SERVER_LIST_KEY);
		if (bandanaValue != null && bandanaValue instanceof Map) {
			sonarServers = (Map<Integer, BambooSonarServer>) bandanaValue;
		} else {
			sonarServers = new HashMap<Integer, BambooSonarServer>();
		}
	}

	/**
	 * Store the {@link BambooSonarServer} {@link Map} using the {@link BandanaManager}
	 */
	private void storeSonarServers() {
		if (bandanaManager == null) {
			throw new IllegalStateException("BandanaManager is not set");
		}
		bandanaManager.setValue(PlanAwareBandanaContext.GLOBAL_CONTEXT, SONAR_SERVER_LIST_KEY, sonarServers);
	}

	/**
	 * Get the next SonarServer Id from Bandana
	 * 
	 * @return the next ServerId
	 */
	private int getNextSonarServerId() {
		if (bandanaManager == null) {
			throw new IllegalStateException("BandanaManager is not set");
		}
		int nextServerId = 1;
		final Object bandanaValue = bandanaManager.getValue(PlanAwareBandanaContext.GLOBAL_CONTEXT,
			NEXT_SONAR_SERVER_ID_KEY);
		if (bandanaValue != null && bandanaValue instanceof Integer) {
			nextServerId = (Integer) bandanaValue;
		}
		bandanaManager.setValue(PlanAwareBandanaContext.GLOBAL_CONTEXT, NEXT_SONAR_SERVER_ID_KEY, nextServerId + 1);
		return nextServerId;
	}

	/**
	 * Get the {@link BambooSonarServer} {@link Collection}
	 * 
	 * @return {@link Collection} of {@link BambooSonarServer} objects
	 */
	public Collection<BambooSonarServer> getSonarServers() {
		return Collections.unmodifiableCollection(sonarServers.values());
	}

}
