<?xml version="1.0" encoding="UTF-8"?>
<!--
 ~ Licensed to Marvelution under one or more contributor license 
 ~ agreements.  See the NOTICE file distributed with this work 
 ~ for additional information regarding copyright ownership.
 ~ Marvelution licenses this file to you under the Apache License,
 ~ Version 2.0 (the "License"); you may not use this file except
 ~ in compliance with the License.
 ~ You may obtain a copy of the License at
 ~
 ~  http://www.apache.org/licenses/LICENSE-2.0
 ~
 ~ Unless required by applicable law or agreed to in writing,
 ~ software distributed under the License is distributed on an
 ~ "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 ~ KIND, either express or implied. See the License for the
 ~ specific language governing permissions and limitations
 ~ under the License.
 -->
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">
	<modelVersion>4.0.0</modelVersion>
	
	<parent>
		<groupId>com.marvelution</groupId>
		<artifactId>marvelution</artifactId>
		<version>9</version>
	</parent>
	<groupId>com.marvelution.bamboo.plugins</groupId>
	<artifactId>bamboo-sonar-plugin</artifactId>
	<version>4.1.0-SNAPSHOT</version>
	<packaging>jar</packaging>
	
	<name>Bamboo Sonar plugin</name>
	<description>Bamboo plugin to integrate Sonar code quality platform with Bamboo</description>
	<url>http://apidocs.marvelution.com/${project.artifactId}/${project.version}/</url>
	<inceptionYear>2009</inceptionYear>
	<organization>
		<name>Marvelution</name>
		<url>http://www.marvelution.com/</url>
	</organization>
	<licenses>
		<license>
			<name>The Apache Software License, Version 2.0</name>
			<url>http://www.apache.org/licenses/LICENSE-2.0.txt</url>
			<distribution>repo</distribution>
		</license>
	</licenses>
	
	<scm>
		<connection>scm:git:git@github.com:Marvelution/bamboo-sonar-plugin.git</connection>
		<developerConnection>scm:git:git@github.com:Marvelution/bamboo-sonar-plugin.git</developerConnection>
		<url>http://fisheye.marvelution.com/browse/bamboo-sonar-plugin</url>
	</scm>
	<issueManagement>
		<system>JIRA</system>
		<url>http://issues.marvelution.com/browse/MARVBAMBOOSONAR</url>
	</issueManagement>
	<ciManagement>
		<system>Bamboo</system>
		<url>http://builds.marvelution.com/browse/MARVBAMBOOSONAR</url>
		<notifiers>
			<notifier>
				<type>mail</type>
				<configuration>
					<address>ci@lists.marvelution.com</address>
				</configuration>
			</notifier>
		</notifiers>
	</ciManagement>
	<distributionManagement>
		<site>
			<id>marvelution.website</id>
			<url>dav:http://repository.marvelution.com/content/sites/apidocs/${project.artifactId}/${project.version}/</url>
		</site>
	</distributionManagement>

	<properties>
		<project.jdk.revision>1.6</project.jdk.revision>
		<atlassian.bamboo.version>2.6.1</atlassian.bamboo.version>
		<sonar.rest.client.version>1.1.0</sonar.rest.client.version>
		<sonar.gadgets.version>1.3.0</sonar.gadgets.version>
		<sonar.gadgets.directory>${project.build.directory}/generated-resources/sonar-gadgets</sonar.gadgets.directory>
		<atlassian.bamboo.plugin.key>${project.groupId}.sonar</atlassian.bamboo.plugin.key>
		<atlassian.plugin.name>${project.name} Gadgets</atlassian.plugin.name>
		<atlassian.plugin.key>${project.groupId}.sonar.gadgets</atlassian.plugin.key>
		<stagingSiteURL>dav:http://repository.marvelution.com/content/sites/apidocs-staging/${project.artifactId}/${project.version}/</stagingSiteURL>
	</properties>
	
	<dependencies>
		<dependency>
			<groupId>com.atlassian.bamboo</groupId>
			<artifactId>atlassian-bamboo-api</artifactId>
			<version>${atlassian.bamboo.version}</version>
			<type>jar</type>
			<scope>provided</scope>
		</dependency>
		<dependency>
			<groupId>com.atlassian.bamboo</groupId>
			<artifactId>atlassian-bamboo-web</artifactId>
			<version>${atlassian.bamboo.version}</version>
			<type>jar</type>
			<scope>provided</scope>
		</dependency>
		<dependency>
			<groupId>com.atlassian.bamboo</groupId>
			<artifactId>atlassian-bamboo-agent-remote</artifactId>
			<version>${atlassian.bamboo.version}</version>
			<type>jar</type>
			<scope>provided</scope>
		</dependency>
		<dependency>
			<groupId>com.atlassian.aws</groupId>
			<artifactId>atlassian-aws</artifactId>
			<version>0.14</version>
			<type>pom</type>
			<scope>provided</scope>
		</dependency>
		<dependency>
			<groupId>com.atlassian.util.concurrent</groupId>
			<artifactId>atlassian-util-concurrent</artifactId>
			<version>0.0.12</version>
			<type>jar</type>
			<scope>provided</scope>
		</dependency>
		<dependency>
			<groupId>com.atlassian.sal</groupId>
			<artifactId>sal-api</artifactId>
			<version>2.0.14</version>
			<type>jar</type>
			<scope>provided</scope>
		</dependency>
		<dependency>
			<groupId>com.atlassian.templaterenderer</groupId>
			<artifactId>atlassian-template-renderer-api</artifactId>
			<version>1.0</version>
			<type>jar</type>
			<scope>provided</scope>
		</dependency>
		<dependency>
			<groupId>com.atlassian.templaterenderer</groupId>
			<artifactId>atlassian-template-renderer-velocity1.6-plugin</artifactId>
			<version>1.0</version>
			<type>jar</type>
			<scope>provided</scope>
		</dependency>
		<dependency>
			<groupId>com.marvelution.gadgets</groupId>
			<artifactId>sonar-gadgets</artifactId>
			<version>${sonar.gadgets.version}</version>
			<type>jar</type>
			<scope>provided</scope>
		</dependency>
		<dependency>
			<groupId>com.marvelution.sonar</groupId>
			<artifactId>sonar-rest-client</artifactId>
			<version>${sonar.rest.client.version}</version>
			<type>jar</type>
			<scope>compile</scope>
		</dependency>
		<dependency>
			<groupId>javax.servlet</groupId>
			<artifactId>servlet-api</artifactId>
			<version>2.4</version>
			<type>jar</type>
			<scope>provided</scope>
		</dependency>
		<dependency>
			<groupId>org.apache.maven</groupId>
			<artifactId>maven-model</artifactId>
			<version>2.0.9</version>
			<type>jar</type>
			<scope>compile</scope>
		</dependency>
		<dependency>
			<groupId>junit</groupId>
			<artifactId>junit</artifactId>
			<version>4.5</version>
			<type>jar</type>
			<scope>test</scope>
		</dependency>
		<dependency>
			<groupId>org.mockito</groupId>
			<artifactId>mockito-all</artifactId>
			<version>1.7</version>
			<type>jar</type>
			<scope>test</scope>
		</dependency>
	</dependencies>

	<build>
		<!-- This will automatically put POM settings into atlassian-plugin.xml when building.  -->
		<resources>
			<resource>
				<directory>src/main/resources</directory>
				<excludes>
					<exclude>atlassian-plugin.xml</exclude>
					<exclude>gadgets/*.xml</exclude>
				</excludes>
			</resource>
			<resource>
				<directory>src/main/resources</directory>
				<filtering>true</filtering>
				<includes>
					<include>atlassian-plugin.xml</include>
					<include>**/*.properties</include>
				</includes>
			</resource>
		</resources>
		<plugins>
			<plugin>
				<groupId>org.apache.felix</groupId>
				<artifactId>maven-bundle-plugin</artifactId>
				<version>2.0.1</version>
				<executions>
					<execution>
						<id>create-manifest</id>
						<goals>
							<goal>manifest</goal>
						</goals>
					</execution>
				</executions>
				<configuration>
					<baseDir>${sonar.gadgets.directory}</baseDir>
					<outputDirectory>${sonar.gadgets.directory}</outputDirectory>
					<manifestLocation>${sonar.gadgets.directory}/META-INF</manifestLocation>
					<instructions>
						<Atlassian-Plugin-Key>${atlassian.plugin.key}</Atlassian-Plugin-Key>
						<Bundle-SymbolicName>${atlassian.plugin.key}</Bundle-SymbolicName>
						<Export-Package>
							com.marvelution.gadgets.sonar,
							com.marvelution.gadgets.sonar.rest,
							com.marvelution.gadgets.sonar.rest.model,
							com.marvelution.gadgets.sonar.servlet
							i18n,
						</Export-Package>
						<Private-Package>
							gadgets,
							images,
							images.gadgets,
							scripts,
							scripts.jQuery,
							scripts.views,
							styles,
							templates,
							templates.rest,
							templates.rest.gadgets
						</Private-Package>
						<Import-Package>
							com.atlassian.bamboo.build,
							com.atlassian.bamboo.bandana,
							com.atlassian.bamboo.configuration,
							com.atlassian.bamboo.resultsummary,
							com.atlassian.bamboo.v2,
							com.atlassian.bamboo.ww2,
							com.atlassian.bamboo.ww2.actions,
							com.atlassian.bamboo.ww2.aware,
							com.atlassian.bamboo.ww2.aware.permissions,
							com.atlassian.bandana,
							com.atlassian.plugin.webresource,
							com.atlassian.plugins.rest.common,
							com.atlassian.plugins.rest.common.security,
							com.atlassian.sal.api.message,
							com.atlassian.sal.api.upgrade,
							com.atlassian.templaterenderer,
							com.atlassian.templaterenderer.velocity.one.six,
							com.marvelution.bamboo.plugins.sonar,
							com.marvelution.bamboo.plugins.sonar.utils,
							com.marvelution.sonar.rest.client,
							com.marvelution.sonar.rest.client.service,
							com.sun.jersey.spi.resource,
							javax.servlet,
							javax.servlet.http,
							javax.xml.bind.annotation;version="2.1",
							javax.ws.rs;version="1.0",
							javax.ws.rs.core;version="1.0",
							javax.ws.rs.ext,
							org.apache.commons.httpclient,
							org.apache.commons.httpclient.auth,
							org.apache.commons.httpclient.methods,
							org.apache.commons.httpclient.params,
							org.apache.commons.lang,
							org.apache.commons.lang.builder;version="2.4",
							org.apache.log4j
						</Import-Package>
						<Include-Resource>
							{maven-resources}
						</Include-Resource>
						<Spring-Context>*;timeout:=60</Spring-Context>
					</instructions>
				</configuration>
			</plugin>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-assembly-plugin</artifactId>
				<version>2.2-beta-4</version>
				<executions>
					<execution>
						<id>create-gadgets-bundle</id>
						<phase>prepare-package</phase>
						<goals>
							<goal>single</goal>
						</goals>
						<configuration>
							<attach>false</attach>
							<archive>
								<manifestFile>${sonar.gadgets.directory}/META-INF/MANIFEST.MF</manifestFile>
							</archive>
							<appendAssemblyId>false</appendAssemblyId>
							<finalName>bamboo-sonar-gadgets-${project.version}</finalName>
							<descriptors>
								<descriptor>src/assembly/gadgets-archive.xml</descriptor>
							</descriptors>
						</configuration>
					</execution>
					<execution>
						<id>create-main-distribution</id>
						<phase>package</phase>
						<goals>
							<goal>single</goal>
						</goals>
						<configuration>
							<attach>true</attach>
							<descriptors>
								<descriptor>src/assembly/distribution.xml</descriptor>
							</descriptors>
						</configuration>
					</execution>
				</executions>
			</plugin>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-dependency-plugin</artifactId>
				<version>2.0</version>
				<executions>
					<execution>
						<id>unpack-sonar-gadgets</id>
						<phase>generate-resources</phase>
						<goals>
							<goal>unpack</goal>
						</goals>
					</execution>
				</executions>
				<configuration>
					<artifactItems>
						<artifactItem>
							<groupId>com.marvelution.gadgets</groupId>
							<artifactId>sonar-gadgets</artifactId>
							<version>${sonar.gadgets.version}</version>
							<type>jar</type>
							<overWrite>true</overWrite>
							<includes>**/**</includes>
						</artifactItem>
					</artifactItems>
					<outputDirectory>${sonar.gadgets.directory}</outputDirectory>
					<overWriteReleases>true</overWriteReleases>
					<overWriteSnapshots>true</overWriteSnapshots>
				</configuration>
			</plugin>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-jar-plugin</artifactId>
				<configuration>
					<excludes>
						<exclude>com/marvelution/bamboo/plugins/sonar/upgrade/**</exclude>
						<exclude>com/marvelution/bamboo/plugins/sonar/servlet/**</exclude>
						<exclude>com/marvelution/bamboo/plugins/sonar/ww2/actions/build/sonar/**</exclude>
						<exclude>com/marvelution/bamboo/plugins/sonar/ww2/actions/admin/sonar/ViewSonarGadgets.*</exclude>
						<exclude>templates/build/sonar/**</exclude>
						<exclude>templates/admin/sonar/viewSonarGadgets.ftl</exclude>
						<exclude>scripts/sonar-gadget**</exclude>
						<exclude>templates/rest/**</exclude>
					</excludes>
				</configuration>
			</plugin>
			<plugin>
		        <groupId>org.codehaus.mojo</groupId>
				<artifactId>xml-maven-plugin</artifactId>
				<version>1.0-beta-3</version>
				<executions>
					<execution>
						<id>merge-atlassian-plugin-xml</id>
						<phase>generate-resources</phase>
						<goals>
							<goal>transform</goal>
						</goals>
					</execution>
				</executions>
				<configuration>
					<forceCreation>true</forceCreation>
					<transformationSets>
						<transformationSet>
							<dir>src/assembly</dir>
							<includes>
								<include>atlassian-plugin.xml</include>
							</includes>
							<stylesheet>${sonar.gadgets.directory}/xslt/merge-atlassian-plugin.xsl</stylesheet>
							<outputDir>${sonar.gadgets.directory}</outputDir>
							<parameters>
								<parameter>
									<name>with</name>
									<value>${sonar.gadgets.directory}/common-atlassian-plugin.xml</value>
								</parameter>
							</parameters>
						</transformationSet>
					</transformationSets>
				</configuration>
			</plugin>
		</plugins>
		<pluginManagement>
			<plugins>
				<!--This plugin's configuration is used to store Eclipse m2e settings only. It has no influence on the Maven build itself.-->
				<plugin>
					<groupId>org.eclipse.m2e</groupId>
					<artifactId>lifecycle-mapping</artifactId>
					<version>1.0.0</version>
					<configuration>
						<lifecycleMappingMetadata>
							<pluginExecutions>
								<pluginExecution>
									<pluginExecutionFilter>
										<groupId>org.codehaus.mojo</groupId>
										<artifactId>xml-maven-plugin</artifactId>
										<versionRange>[1.0-beta-3,)</versionRange>
										<goals>
											<goal>transform</goal>
										</goals>
									</pluginExecutionFilter>
									<action>
										<ignore />
									</action>
								</pluginExecution>
							</pluginExecutions>
						</lifecycleMappingMetadata>
					</configuration>
				</plugin>
			</plugins>
		</pluginManagement>
	</build>

	<developers>
		<developer>
			<id>markrekveld</id>
			<name>Mark Rekveld</name>
			<url>http://www.marvelution.com</url>
			<organization>Marvelution</organization>
			<email>markrekveld@marvelution.com</email>
			<organizationUrl>http://www.marvelution.com</organizationUrl>
			<timezone>+1</timezone>
			<roles>
				<role>Marvelution Member</role>
			</roles>
		</developer>
	</developers>
</project>
